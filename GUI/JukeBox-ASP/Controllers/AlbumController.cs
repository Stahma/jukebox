﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.Controllers {
    public class AlbumController : Controller {
        public AlbumController(IConnection connection) {
            this.connection = connection;
        }

        [Route("[controller]/{id:long}")]
        public async Task<IActionResult> Get(ulong id) {
            var album = await connection.AlbumByIdAsync(id);
            return View(album);
        }

        private IConnection connection;
    }
}
