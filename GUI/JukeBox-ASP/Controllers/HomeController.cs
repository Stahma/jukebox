﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox_ASP.Extensions;
using Microsoft.AspNetCore.Mvc;
using JukeBox;

namespace JukeBox_ASP.Controllers {
    public class HomeController : Controller {
        public HomeController(IConnection connection) {
            this.connection = connection;
        }

        public IActionResult Index() {
            return View();
        }

        public IActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public IActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public IActionResult Error() {
            return View();
        }

        public IActionResult Link(string key) {
            return ViewComponent("Link", new { key = key });
        }

        public async Task<IActionResult> Search(string text, string type, int page = 1) {
            if (type != null && type != "performers" && type != "albums" && type != "tracks") {
                return NotFound();
            }
            if (string.IsNullOrWhiteSpace(text)) {
                text = string.Empty;
            }

            ViewBag.Text = text;
            ViewBag.Type = type;
            ViewBag.Page = page;
            ViewBag.PerPage = 20;

            if (Request.IsAjaxRequest()) {
                switch (type) {
                case "performers":
                    return PartialView("_Performers", await connection.SearchPerformersAsync(text));
                case "albums":
                    return PartialView("_Albums", await connection.SearchAlbumsAsync(text));
                case "tracks":
                    return PartialView("_Tracks", await connection.SearchTracksAsync(text));
                default:
                    break;
                }
            }

            return View(await connection.SearchAllAsync(text));
        }

        private IConnection connection;
    }
}
