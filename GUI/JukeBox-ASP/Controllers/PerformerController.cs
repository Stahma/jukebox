﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.Controllers {
    public class PerformerController : Controller {
        public PerformerController(IConnection connection) {
            this.connection = connection;
        }

        [Route("[controller]/{id:long}")]
        public async Task<IActionResult> Get(ulong id) {
            Performer performer = await connection.PerformerByIdAsync(id);
            if (performer == null) {
                return NotFound();
            }

            return View(performer);
        }

        private IConnection connection;
    }
}
