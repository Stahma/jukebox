﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using JukeBox_ASP.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.Controllers {
    public class GenreController : Controller {
        public GenreController(IConnection connection) {
            this.connection = connection;
        }

        [Route("[controller]")]
        public IActionResult All() {
            return RedirectToAction("Get", new { id = 1 });
        }

        [Route("[controller]/{id:long}")]
        public async Task<IActionResult> Get(ulong id, string type = "performers", int page = 1) {
            ViewBag.Id = id;
            ViewBag.Page = page;
            ViewBag.PerPage = 20;
            ViewBag.Type = type;

            if (Request.IsAjaxRequest()) {
                return PartialView("_Get");
            }

            var genres = await connection.AllGenresAsync();
            return View(genres);
        }

        private IConnection connection;
    }
}
