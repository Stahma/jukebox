﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.ViewComponents {
    public class AlbumViewComponent : ViewComponent {
        public AlbumViewComponent(IConnection connection) {
            this.connection = connection;
        }

        public async Task<IViewComponentResult> InvokeAsync(ulong id) {
            var performer = await connection.PerformerByIdAsync(id);
            var albums = performer.Albums;

            return View(albums);
        }

        private IConnection connection;
    }
}
