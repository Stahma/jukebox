﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.ViewComponents {
    public class AllViewComponent : ViewComponent {
        public IViewComponentResult Invoke(Objects model) {
            return View(model);
        }
    }
}
