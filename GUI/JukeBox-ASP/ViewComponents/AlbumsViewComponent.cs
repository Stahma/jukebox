﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.Views.Shared.Components.Albums {
    public class AlbumsViewComponent : ViewComponent {

        public IViewComponentResult Invoke(IEnumerable<Album> albums, int page, int size) {
            return View(albums.Skip((page - 1) * size).Take(size));
        }
    }
}
