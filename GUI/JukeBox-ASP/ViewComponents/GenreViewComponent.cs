﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.ViewComponents {
    public class GenreViewComponent : ViewComponent {
        public GenreViewComponent(IConnection connection) {
            this.connection = connection;
        }

        public async Task<IViewComponentResult> InvokeAsync(ulong id, string type) {
            var genre = new Genre(id);

            switch (type) {
            case "performers":
                var performers = await connection.PerformersByGenreAsync(genre);
                return View("Performers", performers);
            case "albums":
                var albums = await connection.AlbumsByGenreAsync(genre);
                return View("Albums", albums);
            case "tracks":
                return View("Tracks", await connection.TracksByGenreAsync(genre));
            default:
                break;
            }

            return null;
        }

        private IConnection connection;
    }
}
