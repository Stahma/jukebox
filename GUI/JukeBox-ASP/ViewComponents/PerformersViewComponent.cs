﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.ViewComponents {
    public class PerformersViewComponent : ViewComponent {
        public IViewComponentResult Invoke(IEnumerable<Performer> performers, int page, int size) {
            return View(performers.Skip((page - 1) * size).Take(size));
        }
    }
}
