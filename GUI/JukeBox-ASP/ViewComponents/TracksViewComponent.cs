﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.ViewComponents {
    public class TracksViewComponent : ViewComponent {
        public IViewComponentResult Invoke(IEnumerable<Track> tracks, int page, int size) {
            return View(tracks.Skip((page - 1) * size).Take(size));
        }
    }
}
