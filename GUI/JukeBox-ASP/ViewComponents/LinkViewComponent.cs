﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JukeBox;
using Microsoft.AspNetCore.Mvc;

namespace JukeBox_ASP.ViewComponents {
    public class LinkViewComponent : ViewComponent {
        public LinkViewComponent(IConnection connection) {
            this.connection = connection;
        }

        public async Task<IViewComponentResult> InvokeAsync(string key) {
            return Content(await connection.LinkByKeyAsync(key));
        }

        private IConnection connection;
    }
}
