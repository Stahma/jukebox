#pragma once

#include <memory>
#include <mutex>
#include <unordered_set>

class Request;

class RequestManager {
public:
    RequestManager();

    RequestManager(const RequestManager &) = delete;

    RequestManager &operator=(const RequestManager &) = delete;

    virtual ~RequestManager();

    void StartRequest(std::shared_ptr<Request> request);

    void StopRequest(std::shared_ptr<Request> request);

    void StopAll();

private:
    std::unordered_set<std::shared_ptr<Request>> requests;
    std::mutex mutex;
};


