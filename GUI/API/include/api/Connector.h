#pragma once

#include <boost/asio.hpp>
#include <boost/asio/connect.hpp>
#include <thread>
#include "RequestManager.h"
#include "Request.h"

class Connector : public std::enable_shared_from_this<Connector> {
public:
    Connector();

    Connector(const Connector &) = delete;

    Connector &operator=(const Connector &) = delete;

    void Start(const std::string &address, uint16_t port);

    void Stop();

    void SendRequest(std::string data, ResponseCallback responseCallback, ErrorCallback errorCallback);

private:
    std::shared_ptr<std::thread> ioServiceThread;
    boost::asio::io_service ioService;
    boost::asio::io_service::work work;
    boost::asio::ip::tcp::endpoint endpoint;
    RequestManager requestManager;
};
