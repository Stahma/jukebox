#pragma once

#ifdef __cplusplus
extern "C" {
#endif
	typedef void(*ResponseCallback)(const char *);
	typedef void(*ErrorCallback)(int code, const char *);
#ifdef __cplusplus
}
#endif
