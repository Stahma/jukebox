#pragma once

#include <stdlib.h>
#include <stdint.h>
#include "Callbacks.h"

#if defined _WIN32 || defined __CYGWIN__
#define JUKEBOX_HELPER_DLL_IMPORT __declspec(dllimport)
#define JUKEBOX_HELPER_DLL_EXPORT __declspec(dllexport)
#define JUKEBOX_HELPER_DLL_LOCAL
#else
#if __GNUC__ >= 4
#define JUKEBOX_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
#define JUKEBOX_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
#define JUKEBOX_HELPER_DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#else
#define JUKEBOX_HELPER_DLL_IMPORT
#define JUKEBOX_HELPER_DLL_EXPORT
#define JUKEBOX_HELPER_DLL_LOCAL
#endif
#endif

#ifdef _USRDLL
#ifdef API_EXPORTS
#define JUKEBOX_API JUKEBOX_HELPER_DLL_EXPORT
#else
#define JUKEBOX_API JUKEBOX_HELPER_DLL_IMPORT
#endif
#define JUKEBOX_LOCAL JUKEBOX_HELPER_DLL_LOCAL
#else
#define JUKEBOX_API
#define JUKEBOX_LOCAL
#endif

#ifdef __cplusplus
extern "C" {
#endif
	JUKEBOX_API void Initialize(const char *address, uint16_t port);

	JUKEBOX_API void GetPerformerById(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetAlbumById(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetTrackById(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetGenreById(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetLyricsById(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetLinkByKey(const char *key, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void SearchPerformersByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void SearchAlbumsByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void SearchTracksByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void SearchAllByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetPerformersByGenreId(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetAlbumsByGenreId(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetTracksByGenreId(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetAllByGenreId(uint64_t id, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetPerformersByGenreName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetAlbumsByGenreName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetTracksByGenreName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void GetAllByGenreName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void FindPerformersByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void FindAlbumsByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void FindTracksByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void FindGenreByName(const char *name, ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void AllPerformers(ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void AllAlbums(ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void AllTracks(ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void AllGenres(ResponseCallback responseCallback, ErrorCallback errorCallback);

	JUKEBOX_API void Release();
#ifdef __cplusplus
}
#endif