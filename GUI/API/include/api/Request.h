#pragma once

#include <functional>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/streambuf.hpp>
#include "Callbacks.h"

class RequestManager;

class Request : public std::enable_shared_from_this<Request> {
public:
    Request(std::shared_ptr<boost::asio::ip::tcp::socket> sock, boost::asio::ip::tcp::endpoint endpoint,
            RequestManager &requestManager, std::string data, ResponseCallback responseCallback,
            ErrorCallback errorCallback);

    Request(const Request &) = delete;

    Request &operator=(const Request &) = delete;

    void Start();

    void Stop();

private:
    void connectHandler(const boost::system::error_code &error);

    void writeHandler(const boost::system::error_code &error, size_t transferred);

    void readHandler(const boost::system::error_code &error, size_t transferred);

    ResponseCallback responseCallback;
    ErrorCallback errorCallback;

    boost::asio::ip::tcp::endpoint endpoint;
    std::shared_ptr<boost::asio::ip::tcp::socket> sock;
    boost::asio::streambuf buffer;
    RequestManager &requestManager;
};
