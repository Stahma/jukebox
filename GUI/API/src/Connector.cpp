#include <string>
#include <boost/bind.hpp>
#include "api/Request.h"
#include "api/Connector.h"

Connector::Connector() : ioService(), work(ioService) { }

void Connector::Start(const std::string &address, uint16_t port) {
    boost::asio::ip::address addr = boost::asio::ip::address_v4::from_string(address);

    endpoint.address(addr);
    endpoint.port(port);

    ioServiceThread = std::make_shared<std::thread>(boost::bind(&boost::asio::io_service::run, &ioService));
}

void Connector::Stop() {
    requestManager.StopAll();
    ioService.stop();
    ioServiceThread->join();
}

void Connector::SendRequest(std::string data, ResponseCallback responseCallback, ErrorCallback errorCallback) {
    std::shared_ptr<boost::asio::ip::tcp::socket> sock(new boost::asio::ip::tcp::socket(ioService));

    std::shared_ptr<Request> request(
            new Request(std::move(sock), endpoint, requestManager, data, responseCallback, errorCallback));

    requestManager.StartRequest(std::move(request));
}