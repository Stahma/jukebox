﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JukeBox {
    [DataContract]
    public class Genre {
        public Genre() { }

        public Genre(ulong id) {
            this.id = id;
        }

        public Genre(ulong id, string name, 
            IEnumerable<Performer> performers = null, IEnumerable<Album> albums = null, IEnumerable<Track> tracks = null) {

            this.id = id;
            this.name = name;
            this.performers = performers?.ToArray();
            this.albums = albums?.ToArray();
            this.tracks = tracks?.ToArray();
        }

        #region Public Properties
        public ulong Id => id;

        public string Name => name;

        public IEnumerable<Performer> Performers => performers;

        public IEnumerable<Album> Albums => albums;

        public IEnumerable<Track> Tracks => tracks;
        #endregion

        #region Private Fields
        [DataMember(Name = nameof(Id), IsRequired = true)]
        private ulong id;

        [DataMember(Name = nameof(Name))]
        private string name;

        [DataMember(Name = nameof(Performers))]
        private Performer[] performers;

        [DataMember(Name = nameof(Albums))]
        private Album[] albums;

        [DataMember(Name = nameof(Tracks))]
        private Track[] tracks;
        #endregion
    }
}
