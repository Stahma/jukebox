﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;

namespace JukeBox {
    #region Delegates
    internal delegate void ResponseCallback(string message);

    internal delegate void ErrorCallback(int code, string message);
    #endregion

    public sealed class Connection : IConnection, IDisposable {
        #region Constructors
        public Connection(string address, ushort port) {
            Initialize(address, port);
            disposed = false;
        }
        #endregion

        #region Destructor
        ~Connection() {
            Dispose(false);
        }
        #endregion

        #region Public Methods

        public Task<Performer> PerformerByIdAsync(ulong id) {
            throwIfDisposed();

            TaskCompletionSource<Performer> source = new TaskCompletionSource<Performer>();
            GetPerformerById(
                id,
                (response) => source.SetResult(fromJSON<Performer>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<Album> AlbumByIdAsync(ulong id) {
            throwIfDisposed();

            TaskCompletionSource<Album> source = new TaskCompletionSource<Album>();
            GetAlbumById(
                id,
                (response) => source.SetResult(fromJSON<Album>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<Track> TrackByIdAsync(ulong id) {
            throwIfDisposed();

            TaskCompletionSource<Track> source = new TaskCompletionSource<Track>();
            GetTrackById(
                id,
                (response) => source.SetResult(fromJSON<Track>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<Genre> GenreByIdAsync(ulong id) {
            throwIfDisposed();

            TaskCompletionSource<Genre> source = new TaskCompletionSource<Genre>();
            GetGenreById(
                id,
                (response) => source.SetResult(fromJSON<Genre>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<Lyrics> LyricsByIdAsync(ulong id) {
            throwIfDisposed();

            TaskCompletionSource<Lyrics> source = new TaskCompletionSource<Lyrics>();
            GetLyricsById(
                id,
                (response) => source.SetResult(fromJSON<Lyrics>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<string> LinkByKeyAsync(string key) {
            throwIfDisposed();

            TaskCompletionSource<string> source = new TaskCompletionSource<string>();
            GetLinkByKey(
                key,
                (response) => source.SetResult(response),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Performer>> SearchPerformersAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Performer>> source = new TaskCompletionSource<IEnumerable<Performer>>();
            SearchPerformersByName(
                name,
                (response) => source.SetResult(fromJSON<Performer[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Album>> SearchAlbumsAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Album>> source = new TaskCompletionSource<IEnumerable<Album>>();
            SearchAlbumsByName(
                name,
                (response) => source.SetResult(fromJSON<Album[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }


        public Task<IEnumerable<Track>> SearchTracksAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Track>> source = new TaskCompletionSource<IEnumerable<Track>>();
            SearchTracksByName(
                name,
                (response) => source.SetResult(fromJSON<Track[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }


        public Task<Objects> SearchAllAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<Objects> source = new TaskCompletionSource<Objects>();
            SearchAllByName(
                name,
                (response) => source.SetResult(fromJSON<Objects>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Performer>> FindPerformersAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Performer>> source = new TaskCompletionSource<IEnumerable<Performer>>();
            FindPerformersByName(
                name,
                (response) => source.SetResult(fromJSON<Performer[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Album>> FindAlbumsAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Album>> source = new TaskCompletionSource<IEnumerable<Album>>();
            FindAlbumsByName(
                name,
                (response) => source.SetResult(fromJSON<Album[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Track>> FindTracksAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Track>> source = new TaskCompletionSource<IEnumerable<Track>>();
            FindTracksByName(
                name,
                (response) => source.SetResult(fromJSON<Track[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<Genre> FindGenreAsync(string name) {
            throwIfDisposed();

            TaskCompletionSource<Genre> source = new TaskCompletionSource<Genre>();
            FindGenreByName(
                name,
                (response) => source.SetResult(fromJSON<Genre>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Performer>> PerformersByGenreAsync(Genre genre) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Performer>> source = new TaskCompletionSource<IEnumerable<Performer>>();
            GetPerformersByGenreId(
                genre.Id,
                (response) => source.SetResult(fromJSON<Performer[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Album>> AlbumsByGenreAsync(Genre genre) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Album>> source = new TaskCompletionSource<IEnumerable<Album>>();
            GetAlbumsByGenreId(
                genre.Id,
                (response) => source.SetResult(fromJSON<Album[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Track>> TracksByGenreAsync(Genre genre) {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Track>> source = new TaskCompletionSource<IEnumerable<Track>>();
            GetTracksByGenreId(
                genre.Id,
                (response) => source.SetResult(fromJSON<Track[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<Objects> AllByGenreAsync(Genre genre) {
            throwIfDisposed();

            TaskCompletionSource<Objects> source = new TaskCompletionSource<Objects>();
            GetAllByGenreId(
                genre.Id,
                (response) => source.SetResult(fromJSON<Objects>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Performer>> AllPerformersAsync() {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Performer>> source = new TaskCompletionSource<IEnumerable<Performer>>();
            AllPerformers(
                (response) => source.SetResult(fromJSON<Performer[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Album>> AllAlbumsAsync() {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Album>> source = new TaskCompletionSource<IEnumerable<Album>>();
            AllAlbums(
                (response) => source.SetResult(fromJSON<Album[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Track>> AllTracksAsync() {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Track>> source = new TaskCompletionSource<IEnumerable<Track>>();
            AllTracks(
                (response) => source.SetResult(fromJSON<Track[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }

        public Task<IEnumerable<Genre>> AllGenresAsync() {
            throwIfDisposed();

            TaskCompletionSource<IEnumerable<Genre>> source = new TaskCompletionSource<IEnumerable<Genre>>();
            AllGenres(
                (response) => source.SetResult(fromJSON<Genre[]>(response)),
                (code, message) => source.SetException(new JukeboxInternalException(code, message)));

            return source.Task;
        }
        #endregion

        #region Dispose
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) { }

                Release();
                disposed = true;
            }
        }
        #endregion

        #region Imported Methods
        [DllImport("jukebox-api.dll")]
        private static extern void Initialize(string address, ushort port);

        [DllImport("jukebox-api.dll")]
        private static extern void GetPerformerById(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetAlbumById(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetTrackById(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetGenreById(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetLyricsById(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetLinkByKey(string key, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void SearchPerformersByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void SearchAlbumsByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void SearchTracksByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void SearchAllByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void FindPerformersByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void FindAlbumsByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void FindTracksByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void FindGenreByName(string name, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetPerformersByGenreId(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetAlbumsByGenreId(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetTracksByGenreId(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void GetAllByGenreId(ulong id, ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void AllPerformers(ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void AllAlbums(ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void AllTracks(ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void AllGenres(ResponseCallback callback, ErrorCallback errorCallback);

        [DllImport("jukebox-api.dll")]
        private static extern void Release();
        #endregion

        #region Private Methods
        void throwIfDisposed() {
            if (disposed) {
                throw new ObjectDisposedException("Cannot access a disposed object.");
            }
        }

        static T fromJSON<T>(string input) where T : class {
            try {
                return JsonConvert.DeserializeObject<T>(input);
            } catch (JsonSerializationException exception) {
                Console.Error.Write(exception);
            }

            return null;
        }

        static T fromJSON<T>(string input, T anonymous) where T : class {
            try {
                return JsonConvert.DeserializeAnonymousType(input, anonymous);
            } catch (JsonSerializationException exception) {
                Console.Error.Write(exception);
            }

            return null;
        }
        #endregion

        #region Private Fields
        private bool disposed;
        #endregion
    }
}
