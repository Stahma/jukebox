﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace JukeBox {
    [DataContract]
    public class Objects {
        public Objects(IEnumerable<Performer> performers = null, IEnumerable<Album> albums = null, IEnumerable<Track> tracks = null) {
            this.performers = performers?.ToArray();
            this.albums = albums?.ToArray();
            this.tracks = tracks?.ToArray();
        }

        public IEnumerable<Performer> Performers => performers;

        public IEnumerable<Album> Albums => albums;

        public IEnumerable<Track> Tracks => tracks;

        [DataMember(Name = nameof(Performers))]
        private Performer[] performers;

        [DataMember(Name = nameof(Albums))]
        private Album[] albums;

        [DataMember(Name = nameof(Tracks))]
        private Track[] tracks;
    }
}
