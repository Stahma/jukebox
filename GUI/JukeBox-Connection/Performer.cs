﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JukeBox {
    [DataContract]
    public class Performer {
        #region Public Properties
        public ulong Id => id;

        public string Name => name;

        public string Description => description;

        public string Photo => photo;

        public IEnumerable<Album> Albums => albums;

        public IEnumerable<Track> Tracks => tracks;

        public IEnumerable<Genre> Genres => genres;
        #endregion

        #region Private Fields
        [DataMember(Name = nameof(Id), IsRequired = true)]
        private ulong id;

        [DataMember(Name = nameof(Name))]
        private string name;

        [DataMember(Name = nameof(Description))]
        private string description;

        [DataMember(Name = nameof(Photo))]
        private string photo;

        [DataMember(Name = nameof(Albums))]
        private Album[] albums;

        [DataMember(Name = nameof(Tracks))]
        private Track[] tracks;

        [DataMember(Name = nameof(Genres))]
        private Genre[] genres;
        #endregion
    }
}
