﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JukeBox {
    [Serializable]
    public class JukeboxInternalException : Exception {
        public int Code { get; }

        public JukeboxInternalException() { }

        public JukeboxInternalException(int code, string message) : base(message) {
            Code = code;
        }
        public JukeboxInternalException(int code, string message, Exception inner) : base(message, inner) {
            Code = code;
        }

        protected JukeboxInternalException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
