﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JukeBox {
    [DataContract]
    public class Track {
        #region Public Properties
        public ulong Id => id;

        public string Name => name;

        public string Key => key;

        public Lyrics Lyrics => lyrics;

        public Performer Performer => performer;

        public Album Album => album;

        public IEnumerable<Genre> Genres => genres;
        #endregion

        #region Private Fields
        [DataMember(Name = nameof(Id), IsRequired = true)]
        private ulong id;

        [DataMember(Name = nameof(Name))]
        private string name;

        [DataMember(Name = nameof(Key))]
        private string key;

        [DataMember(Name = nameof(Lyrics))]
        private Lyrics lyrics;

        [DataMember(Name = nameof(Performer))]
        private Performer performer;

        [DataMember(Name = nameof(Album))]
        private Album album;

        [DataMember(Name = nameof(Genres))]
        private Genre[] genres;
        #endregion
    }
}
