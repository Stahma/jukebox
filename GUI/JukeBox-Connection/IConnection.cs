﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JukeBox {
    public interface IConnection {
        Task<Performer> PerformerByIdAsync(ulong id);

        Task<Album> AlbumByIdAsync(ulong id);

        Task<Track> TrackByIdAsync(ulong id);

        Task<Genre> GenreByIdAsync(ulong id);

        Task<Lyrics> LyricsByIdAsync(ulong id);

        Task<string> LinkByKeyAsync(string key);

        Task<IEnumerable<Performer>> SearchPerformersAsync(string name);

        Task<IEnumerable<Album>> SearchAlbumsAsync(string name);

        Task<IEnumerable<Track>> SearchTracksAsync(string name);

        Task<Objects> SearchAllAsync(string name);

        Task<IEnumerable<Performer>> FindPerformersAsync(string name);

        Task<IEnumerable<Album>> FindAlbumsAsync(string name);

        Task<IEnumerable<Track>> FindTracksAsync(string name);

        Task<Genre> FindGenreAsync(string name);

        Task<IEnumerable<Performer>> PerformersByGenreAsync(Genre genre);

        Task<IEnumerable<Album>> AlbumsByGenreAsync(Genre genre);

        Task<IEnumerable<Track>> TracksByGenreAsync(Genre genre);

        Task<Objects> AllByGenreAsync(Genre genre);

        Task<IEnumerable<Performer>> AllPerformersAsync();

        Task<IEnumerable<Album>> AllAlbumsAsync();

        Task<IEnumerable<Track>> AllTracksAsync();

        Task<IEnumerable<Genre>> AllGenresAsync();
    }
}
