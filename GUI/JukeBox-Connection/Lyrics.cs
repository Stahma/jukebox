﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JukeBox {
    [DataContract]
    public class Lyrics {
        #region Public Properties
        public ulong Id => id;
        public string Text => text;
        #endregion

        #region Private Fields
        [DataMember(Name = nameof(Id), IsRequired = true)]
        private ulong id;

        [DataMember(Name = nameof(Text))]
        private string text;
        #endregion
    }
}
