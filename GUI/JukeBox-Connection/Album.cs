﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace JukeBox {
    [DataContract]
    public class Album {
        #region Public Properties
        public ulong Id => id;

        public string Name => name;

        public DateTime Date => date;

        public string Photo => photo;

        public string Label => label;

        public Performer Performer => performer;

        public IEnumerable<Track> Tracks => tracks;

        public IEnumerable<Genre> Genres => genres;
        #endregion

        #region Private Fields
        [DataMember(Name = nameof(Id), IsRequired = true)]
        private ulong id;

        [DataMember(Name = nameof(Name))]
        private string name;

        [DataMember(Name = nameof(Date))]
        private DateTime date;

        [DataMember(Name = nameof(Photo))]
        private string photo;

        [DataMember(Name = nameof(Label))]
        private string label;

        [DataMember(Name = nameof(Performer))]
        private Performer performer;

        [DataMember(Name = nameof(Tracks))]
        private Track[] tracks;

        [DataMember(Name = nameof(Genres))]
        private Genre[] genres;
        #endregion
    }
}
