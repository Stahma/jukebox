#include "dbservice/DatabaseService.h"

template<typename T>
static std::string ToString(std::shared_ptr<T> object) {
    if (object == nullptr) {
        return "{}";
    }

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    object->Serialize(writer, T::ALL);

    return buffer.GetString();
}

template<typename T>
static std::string ToString(std::vector<std::shared_ptr<T>> objects) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartArray();
    {
        for (std::shared_ptr<T> o : objects) {
            o->Serialize(writer, T::BASIC);
        }
    }
    writer.EndArray();

    return buffer.GetString();
}

template<typename T>
static std::string ToString(std::vector<std::weak_ptr<T>> objects) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartArray();
    {
        for (std::weak_ptr<T> o : objects) {
            o.lock()->Serialize(writer, T::BASIC);
        }
    }
    writer.EndArray();

    return buffer.GetString();
}

DatabaseService::DatabaseService(std::shared_ptr<RepositoryContainer> container) : container(std::move(container)) { }

DatabaseService::~DatabaseService() { }

std::string DatabaseService::GetPerformer(std::string data) {
    std::shared_ptr<Performer> o = objectById<Performer>(data, container->PerformerRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::GetAlbum(std::string data) {
    std::shared_ptr<Album> o = objectById<Album>(data, container->AlbumRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::GetTrack(std::string data) {
    std::shared_ptr<Track> o = objectById<Track>(data, container->TrackRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::GetGenre(std::string data) {
    std::shared_ptr<Genre> o = objectById<Genre>(data, container->GenreRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::GetLyrics(std::string data) {
    std::shared_ptr<Lyrics> o = objectById<Lyrics>(data, container->LyricsRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::SearchPerformer(std::string data) {
    auto o = searchObjectsByName<Performer>(std::move(data), container->PerformerRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::SearchAlbum(std::string data) {
    auto o = searchObjectsByName<Album>(std::move(data), container->AlbumRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::SearchTrack(std::string data) {
    auto o = searchObjectsByName<Track>(std::move(data), container->TrackRepository());
    return ToString(std::move(o));
}


std::string DatabaseService::SearchAll(std::string data) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        std::string performers = SearchPerformer(data);
        std::string albums = SearchAlbum(data);
        std::string tracks = SearchTrack(data);

        writer.Key("Performers");
        writer.RawValue(performers.c_str(), performers.size(), rapidjson::kArrayType);

        writer.Key("Albums");
        writer.RawValue(albums.c_str(), albums.size(), rapidjson::kArrayType);

        writer.Key("Tracks");
        writer.RawValue(tracks.c_str(), tracks.size(), rapidjson::kArrayType);
    }
    writer.EndObject();

    return buffer.GetString();
}


std::string DatabaseService::FindPerformer(std::string data) {
    auto o = findObjectsByName<Performer>(std::move(data), container->PerformerRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::FindAlbum(std::string data) {
    auto o = findObjectsByName<Album>(std::move(data), container->AlbumRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::FindTrack(std::string data) {
    auto o = findObjectsByName<Track>(std::move(data), container->TrackRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::FindGenre(std::string data) {
    auto o = objectByName<Genre>(std::move(data), container->GenreRepository());
    return ToString(std::move(o));
}


std::string DatabaseService::AllPerformer(std::string/* data*/) {
    return ToString(container->PerformerRepository()->GetAll());
}

std::string DatabaseService::AllAlbum(std::string/* data*/) {
    return ToString(container->AlbumRepository()->GetAll());
}

std::string DatabaseService::AllTrack(std::string/* data*/) {
    return ToString(container->TrackRepository()->GetAll());
}

std::string DatabaseService::AllGenre(std::string/* data*/) {
    return ToString(container->GenreRepository()->GetAll());
}


std::string DatabaseService::GenrePerformer(std::string data) {
    std::shared_ptr<Genre> o = objectById<Genre>(data, container->GenreRepository());
    return ToString(o->Performers());
}

std::string DatabaseService::GenreAlbum(std::string data) {
    std::shared_ptr<Genre> o = objectById<Genre>(data, container->GenreRepository());
    return ToString(o->Albums());
}

std::string DatabaseService::GenreTrack(std::string data) {
    std::shared_ptr<Genre> o = objectById<Genre>(data, container->GenreRepository());
    return ToString(o->Tracks());
}

std::string DatabaseService::GenreAll(std::string data) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        std::string performers = GenrePerformer(data);
        std::string albums = GenreAlbum(data);
        std::string tracks = GenreTrack(data);

        writer.Key("Performers");
        writer.RawValue(performers.c_str(), performers.size(), rapidjson::kArrayType);

        writer.Key("Albums");
        writer.RawValue(albums.c_str(), albums.size(), rapidjson::kArrayType);

        writer.Key("Tracks");
        writer.RawValue(tracks.c_str(), tracks.size(), rapidjson::kArrayType);
    }
    writer.EndObject();

    return buffer.GetString();
}

std::string DatabaseService::WritePerformer(std::string data) {
    std::shared_ptr<Performer> o = writeObject<Performer>(std::move(data), container->PerformerRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::WriteAlbum(std::string data) {
    std::shared_ptr<Album> o = writeObject<Album>(std::move(data), container->AlbumRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::WriteTrack(std::string data) {
    std::shared_ptr<Track> o = writeObject<Track>(std::move(data), container->TrackRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::WriteGenre(std::string data) {
    std::shared_ptr<Genre> o = writeObject<Genre>(std::move(data), container->GenreRepository());
    return ToString(std::move(o));
}

std::string DatabaseService::WriteLyrics(std::string data) {
    std::shared_ptr<Lyrics> o = writeObject<Lyrics>(std::move(data), container->LyricsRepository());
    return ToString(std::move(o));
}