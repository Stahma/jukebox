#pragma once

#include <string>

class IDatabaseService {
public:
    IDatabaseService() { }

    virtual ~IDatabaseService() { }

    virtual std::string GetPerformer(std::string data) = 0;

    virtual std::string GetAlbum(std::string data) = 0;

    virtual std::string GetTrack(std::string data) = 0;

    virtual std::string GetGenre(std::string data) = 0;

    virtual std::string GetLyrics(std::string data) = 0;

    virtual std::string SearchPerformer(std::string data) = 0;

    virtual std::string SearchAlbum(std::string data) = 0;

    virtual std::string SearchTrack(std::string data) = 0;

    virtual std::string SearchAll(std::string data) = 0;

    virtual std::string FindPerformer(std::string data) = 0;

    virtual std::string FindAlbum(std::string data) = 0;

    virtual std::string FindTrack(std::string data) = 0;

    virtual std::string FindGenre(std::string data) = 0;

    virtual std::string AllPerformer(std::string data) = 0;

    virtual std::string AllAlbum(std::string data) = 0;

    virtual std::string AllTrack(std::string data) = 0;

    virtual std::string AllGenre(std::string data) = 0;

    virtual std::string GenrePerformer(std::string data) = 0;

    virtual std::string GenreAlbum(std::string data) = 0;

    virtual std::string GenreTrack(std::string data) = 0;

    virtual std::string GenreAll(std::string data) = 0;

    virtual std::string WritePerformer(std::string data) = 0;

    virtual std::string WriteAlbum(std::string data) = 0;

    virtual std::string WriteTrack(std::string data) = 0;

    virtual std::string WriteGenre(std::string data) = 0;

    virtual std::string WriteLyrics(std::string data) = 0;
};