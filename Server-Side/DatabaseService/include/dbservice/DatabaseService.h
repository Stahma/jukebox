#pragma once

#include "IDatabaseService.h"
#include <database/Models-odb.hxx>
#include <repository/IPerformerRepository.h>

#include <rapidjson/writer.h>
#include <repository/IAlbumRepository.h>
#include <repository/IGenreRepository.h>
#include <repository/ITrackRepository.h>
#include <repository/ILyricsRepository.h>
#include <repository/RepositoryContainer.h>

class DatabaseService : public IDatabaseService {
public:
    DatabaseService(std::shared_ptr<RepositoryContainer> container);

    DatabaseService(const DatabaseService &service) = delete;

    DatabaseService operator=(const DatabaseService &service) = delete;

    virtual ~DatabaseService();

    virtual std::string GetPerformer(std::string data) override;

    virtual std::string GetAlbum(std::string data) override;

    virtual std::string GetTrack(std::string data) override;

    virtual std::string GetGenre(std::string data) override;

    virtual std::string GetLyrics(std::string data) override;

    virtual std::string SearchPerformer(std::string data) override;

    virtual std::string SearchAlbum(std::string data) override;

    virtual std::string SearchTrack(std::string data) override;

    virtual std::string SearchAll(std::string data) override;

    virtual std::string FindPerformer(std::string data) override;

    virtual std::string FindAlbum(std::string data) override;

    virtual std::string FindTrack(std::string data) override;

    virtual std::string FindGenre(std::string data) override;

    virtual std::string AllPerformer(std::string data) override;

    virtual std::string AllAlbum(std::string data) override;

    virtual std::string AllTrack(std::string data) override;

    virtual std::string AllGenre(std::string data) override;

    virtual std::string GenrePerformer(std::string data) override;

    virtual std::string GenreAlbum(std::string data) override;

    virtual std::string GenreTrack(std::string data) override;

    virtual std::string GenreAll(std::string data) override;

    virtual std::string WritePerformer(std::string data) override;

    virtual std::string WriteAlbum(std::string data) override;

    virtual std::string WriteTrack(std::string data) override;

    virtual std::string WriteGenre(std::string data) override;

    virtual std::string WriteLyrics(std::string data) override;

private:
    template<typename T, typename S>
    std::shared_ptr<T> objectById(std::string data, std::shared_ptr<S> set);

    template<typename T, typename S>
    std::shared_ptr<T> objectByName(std::string data, std::shared_ptr<S> set);

    template<typename T, typename S>
    std::vector<std::shared_ptr<T>> searchObjectsByName(std::string data, std::shared_ptr<S> set);

    template<typename T, typename S>
    std::vector<std::shared_ptr<T>> findObjectsByName(std::string data, std::shared_ptr<S> set);

    template<typename T, typename S>
    std::shared_ptr<T> writeObject(std::string data, std::shared_ptr<S> set);

    template<typename T>
    static bool getMember(std::string data, std::string name, T &member);

    std::shared_ptr<RepositoryContainer> container;
};

/*Don't move methods below to .cpp file. It won't link.*/

template<typename T, typename S>
std::shared_ptr<T> DatabaseService::objectById(std::string data, std::shared_ptr<S> set) {
    uint64_t id;
    if (getMember(std::move(data), "Id", id)) {
        return set->GetById(id);
    }

    return nullptr;
}

template<typename T, typename S>
std::shared_ptr<T> DatabaseService::objectByName(std::string data, std::shared_ptr<S> set) {
    std::string name;
    if (getMember(std::move(data), "Name", name)) {
        return set->FindByName(name);
    }

    return nullptr;
}

template<typename T, typename S>
std::vector<std::shared_ptr<T>> DatabaseService::searchObjectsByName(std::string data, std::shared_ptr<S> set) {
    std::string name;
    if (getMember(std::move(data), "Name", name)) {
        return set->SearchByName(name);
    }

    return { };
}

template<typename T, typename S>
std::vector<std::shared_ptr<T>> DatabaseService::findObjectsByName(std::string data, std::shared_ptr<S> set) {
    std::string name;
    if (getMember(std::move(data), "Name", name)) {
        return set->FindByName(name);
    }

    return { };
}

template<typename T, typename S>
std::shared_ptr<T> DatabaseService::writeObject(std::string data, std::shared_ptr<S> set) {
    rapidjson::Document document;
    document.Parse(data);

    if (document.HasParseError()) {
        std::cerr << "Parsing error. Offset: " << document.GetErrorOffset() << std::endl;
        return false;
    }

    std::shared_ptr<T> object = nullptr;
    try {
        object = T::Deserialize(document);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return nullptr;
    }

    if (set->SaveEntity(object)) {
        object = set->GetById(object->Id());
    }

    return object;
}

template<typename T>
bool DatabaseService::getMember(std::string data, std::string name, T &member) {
    rapidjson::Document document;
    document.Parse(data);

    if (document.HasParseError()) {
        std::cerr << "Error: parsing error at " << document.GetErrorOffset() << std::endl;
        return false;
    }

    rapidjson::Value::ConstMemberIterator iterator = document.FindMember(name);
    if (iterator == document.MemberEnd()) {
        std::cerr << "Error: " << name << " required." << std::endl;
        return false;
    }

    if (!iterator->value.Is<T>()) {
        std::cerr << "Error: wrong type." << std::endl;
        return false;
    }

    member = iterator->value.Get<T>();
    return true;
}