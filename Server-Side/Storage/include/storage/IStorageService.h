#pragma once

#include <string>

class IStorageService {
public:
    IStorageService() { }

    virtual ~IStorageService() { }

    virtual std::string WriteLink(std::string data) = 0;

    virtual std::string GetLink(std::string data) = 0;
};
