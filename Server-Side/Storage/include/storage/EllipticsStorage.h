#include "IStorageService.h"
#include <memory>
#include <rapidjson/writer.h>
#include <elliptics/session.hpp>
#include <base64/Base64.h>

class EllipticsStorage : public IStorageService {
public:
    EllipticsStorage(const std::string &config);

    EllipticsStorage(const EllipticsStorage &connector) = delete;

    EllipticsStorage &operator=(const EllipticsStorage &connector) = delete;

    virtual ~EllipticsStorage();

    std::string WriteLink(std::string data);

    std::string GetLink(std::string data);

private:
    template<typename T>
    static std::string handleResult(ioremap::elliptics::async_result<T> &result);

    static std::string handleError();

    template<typename T>
    static bool getMember(std::string data, std::string name, T &member);

    dnet_config dconfig;
    std::shared_ptr<ioremap::elliptics::session> session;
    std::shared_ptr<ioremap::elliptics::node> node;
    std::shared_ptr<ioremap::elliptics::file_logger> log;
    Base64 encoder;
};

template<typename T>
std::string EllipticsStorage::handleResult(ioremap::elliptics::async_result<T> &result) {
    std::string link;

    T entry = result.get_one();
    if (result.error()) {
        fprintf(stderr, "Error. %s.\n", result.error().message().c_str());
    } else {

        char buffer[256];
        snprintf(buffer, 256, "//%s%s:%lu:%lu", ioremap::elliptics::address(*(entry.storage_address())).host().c_str(),
                 entry.file_path(), entry.file_info()->offset, entry.file_info()->size);

        link = buffer;
    }

    return link;
}

template<typename T>
bool EllipticsStorage::getMember(std::string data, std::string name, T &member) {
    rapidjson::Document document;
    document.Parse(data);

    if (document.HasParseError()) {
        std::cerr << "Error: parsing error at " << document.GetErrorOffset() << std::endl;
        return false;
    }

    rapidjson::Value::ConstMemberIterator iterator = document.FindMember(name);
    if (iterator == document.MemberEnd()) {
        std::cerr << "Error: " << name << " required." << std::endl;
        return false;
    }

    if (!iterator->value.Is<T>()) {
        std::cerr << "Error: Wrong type." << std::endl;
        return false;
    }

    member = iterator->value.Get<T>();
    return true;
}




