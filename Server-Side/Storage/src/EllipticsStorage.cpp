#define RAPIDJSON_HAS_STDSTRING 1

#include <vector>
#include <iostream>

#include <elliptics/packet.h>
#include <elliptics/session.hpp>

#include <rapidjson/document.h>

#include "storage/EllipticsStorage.h"

EllipticsStorage::EllipticsStorage(const std::string &config) : IStorageService(), encoder(Base64::StdEncoding()) {
    rapidjson::Document doc;
    doc.Parse(config);

    if (doc.HasParseError()) {
        fprintf(stderr, "Parsing error. Offset: %lu\n", doc.GetErrorOffset());
        throw std::runtime_error("Invalid config.");
    }

    dnet_log_level logLevel = DNET_LOG_DEBUG;
    std::string logFile = "dev/stderr";


    rapidjson::Value::ConstMemberIterator logger = doc.FindMember("logger");
    if (logger != doc.MemberEnd()) {
        rapidjson::Value::ConstMemberIterator level = logger->value.FindMember("log_level");
        rapidjson::Value::ConstMemberIterator path = logger->value.FindMember("path");

        if (level != logger->value.MemberEnd()) {
            logLevel = ioremap::elliptics::file_logger::parse_level(level->value.GetString());
        }

        if (path != logger->value.MemberEnd()) {
            logFile = path->value.GetString();
        }
    }

    log = std::make_shared<ioremap::elliptics::file_logger>(logFile.c_str(), logLevel);

    memset(&dconfig, 0, sizeof(dnet_config));
    dconfig.wait_timeout = dconfig.check_timeout = 60;
    dconfig.indexes_shard_count = 2;

    rapidjson::Value::ConstMemberIterator options = doc.FindMember("options");
    if (options != doc.MemberEnd()) {
        rapidjson::Value::ConstMemberIterator waitTimeout = options->value.FindMember("wait_timeout");
        rapidjson::Value::ConstMemberIterator checkTimeout = logger->value.FindMember("check_timeout");
        rapidjson::Value::ConstMemberIterator indexesShardCount = logger->value.FindMember("indexes_shard_count");

        if (waitTimeout != options->value.MemberEnd()) {
            dconfig.wait_timeout = waitTimeout->value.GetInt();
        }

        if (checkTimeout != options->value.MemberEnd()) {
            dconfig.check_timeout = waitTimeout->value.GetInt();
        }

        if (indexesShardCount != options->value.MemberEnd()) {
            dconfig.indexes_shard_count = indexesShardCount->value.GetInt();
        }
    }

    node = std::make_shared<ioremap::elliptics::node>(dnet_logger(*log, blackhole::log::attributes_t()), dconfig);

    rapidjson::Value::ConstMemberIterator remote = doc.FindMember("remote");
    if (remote == doc.MemberEnd()) {
        fprintf(stderr, "Error. Remote addresses are required.\n");
        throw std::runtime_error("Invalid config.");
    }

    for (const rapidjson::Value &a : remote->value.GetArray()) {
        node->add_remote(a.GetString());
    }

    session = std::make_shared<ioremap::elliptics::session>(*node);
    session->set_checker(ioremap::elliptics::checkers::quorum);
    session->set_error_handler(ioremap::elliptics::error_handlers::remove_on_fail(*session));
    session->set_exceptions_policy(ioremap::elliptics::session::no_exceptions);

    rapidjson::Value::ConstMemberIterator groups = doc.FindMember("groups");
    if (groups == doc.MemberEnd()) {
        fprintf(stderr, "Error. Groups are required.\n");
        throw std::runtime_error("Invalid config.");
    }

    std::vector<int> jgroups;
    for (const rapidjson::Value &g : groups->value.GetArray()) {
        jgroups.emplace_back(g.GetInt());
    }
    session->set_groups(jgroups);
}

EllipticsStorage::~EllipticsStorage() {
    session.reset();
    node.reset();
    log.reset();
}

std::string EllipticsStorage::WriteLink(std::string data) {
    std::string key;
    if (!getMember(data, "Key", key)) {
        return handleError();
    }

    std::string base64data;
    if (!getMember(data, "Data", base64data)) {
        return handleError();
    }

    ioremap::elliptics::data_pointer pointer;
    try {
        size_t size = encoder.DecodedLength(base64data.size());
        pointer = ioremap::elliptics::data_pointer::allocate(size);
        encoder.DecodeString((uint8_t *) pointer.data(), base64data);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return handleError();
    }

    ioremap::elliptics::argument_data arg(pointer);

    ioremap::elliptics::async_write_result result = session->write_data(key, arg, 0);

    return handleResult(result);
}

std::string EllipticsStorage::GetLink(std::string data) {
    std::string key;
    if (!getMember(data, "Key", key)) {
        return handleError();
    }

    ioremap::elliptics::async_lookup_result result = session->lookup(key);

    return handleResult(result);
}

std::string EllipticsStorage::handleError() {
    return "";
}