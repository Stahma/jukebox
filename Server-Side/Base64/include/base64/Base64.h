#pragma once

#include <string>

class CorruptInputException : std::exception {
public:
    CorruptInputException(size_t postion) {
        snprintf(message, sizeof(message) / sizeof(*message) - 1, "Illegal base64 data at input byte %lu", postion);
    }

    virtual const char *what() const noexcept override {
        return message;
    }

private:
    char message[64];
};

class Base64 {
public:
    static Base64 StdEncoding();

    static Base64 URLEncoding();

    static Base64 RawStdEncoding();

    static Base64 RawURLEncoding();

    static Base64 NewEncoding(std::string encoder);

    Base64 WithPadding(uint8_t padChar);

    size_t EncodedLength(size_t n);

    size_t DecodedLength(size_t n);

    void Encode(uint8_t *dst, const uint8_t *src, size_t size);

    std::string EncodeToString(const uint8_t *src, size_t size);

    size_t Decode(uint8_t *dst, const uint8_t *src, size_t size);

    size_t DecodeString(uint8_t *dst, const std::string &s);

    const static uint8_t StdPadding;
    const static uint8_t NoPadding;
private:
    Base64(std::string encoder, uint8_t padChar = StdPadding);

    uint8_t encode[64];
    uint8_t decodeMap[256];
    uint8_t padChar;

    const static std::string encodeStd;
    const static std::string encodeURL;
};
