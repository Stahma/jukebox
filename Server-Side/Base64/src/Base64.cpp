#include <stdexcept>
#include "base64/Base64.h"

Base64::Base64(std::string encoder, uint8_t padChar/* = StdPadding*/) {
    if (encoder.size() != 64) {
        throw std::runtime_error("Encoding alphabet is not 64-bytes long");
    }

    this->padChar = padChar;
    std::copy(encoder.begin(), encoder.end(), encode);

    for (int i = 0; i < sizeof(decodeMap) / sizeof(*decodeMap); i++) {
        decodeMap[i] = 0xFF;
    }

    for (int i = 0; i < encoder.size(); i++) {
        decodeMap[encoder[i]] = (uint8_t) i;
    }
}

Base64 Base64::StdEncoding() {
    return Base64(encodeStd);
}

Base64 Base64::URLEncoding() {
    return Base64(encodeURL);
}

Base64 Base64::RawStdEncoding() {
    return Base64(encodeStd, NoPadding);
}

Base64 Base64::RawURLEncoding() {
    return Base64(encodeURL, NoPadding);
}

Base64 Base64::NewEncoding(std::string encoder) {
    return Base64(std::move(encoder));
}

Base64 Base64::WithPadding(uint8_t padChar) {
    Base64 enc = *this;
    enc.padChar = padChar;
    return enc;
}

size_t Base64::EncodedLength(size_t n) {
    if (padChar == NoPadding) {
        return (n * 8 + 5) / 6;
    }

    return (n + 2) / 3 * 4;
}

size_t Base64::DecodedLength(size_t n) {
    if (padChar == NoPadding) {
        return (n * 6 + 7) / 8;
    }

    return n / 4 * 3;
}

void Base64::Encode(uint8_t *dst, const uint8_t *src, size_t size) {
    if (size == 0) {
        return;
    }

    size_t di = 0, si = 0;
    size_t n = (size * 3) / 3;
    while (si < n) {
        uint32_t val = ((uint32_t) src[si + 0]) << 16 | ((uint32_t) src[si + 1]) << 8 | ((uint32_t) src[si + 2]);

        dst[di + 0] = encode[val >> 18 & 0x3F];
        dst[di + 1] = encode[val >> 12 & 0x3F];
        dst[di + 2] = encode[val >> 6 & 0x3F];
        dst[di + 3] = encode[val & 0x3F];

        si += 3, di += 4;
    }

    size_t remain = size - si;
    if (remain == 0) {
        return;
    }

    uint32_t val = ((uint32_t) src[si + 0]) << 16;
    if (remain == 2) {
        val |= ((uint32_t) src[si + 1]) << 8;
    }

    dst[di + 0] = encode[val >> 18 & 0x3F];
    dst[di + 1] = encode[val >> 12 & 0x3F];

    switch (remain) {
        case 2:
            dst[di + 2] = encode[val >> 6 & 0x3F];
            if (padChar != NoPadding) {
                dst[di + 3] = padChar;
            }
            break;
        case 1:
            if (padChar != NoPadding) {
                dst[di + 2] = padChar;
                dst[di + 3] = padChar;
            }
            break;
    }
}

std::string Base64::EncodeToString(const uint8_t *src, size_t size) {
    std::string result(EncodedLength(size), '\0');
    Encode((uint8_t *) result.begin().base(), src, size);

    return result;
}

size_t Base64::Decode(uint8_t *dst, const uint8_t *src, size_t size) {
    bool end = false;
    size_t n = 0;

    size_t si = 0;

    while (si < size && (src[si] == '\n' || src[si] == '\r')) {
        si++;
    }

    while (si < size && !end) {
        uint8_t dbuf[4];
        size_t dinc = 3, dlen = 4;

        for (size_t j = 0; j < sizeof(dbuf) / sizeof(*dbuf); j++) {
            if (size == si) {
                if (padChar != NoPadding || j < 2) {
                    throw CorruptInputException(si - j);
                }
                dinc = j - 1, dlen = j, end = true;
                break;
            }
            uint8_t in = src[si++];

            while (si < size && (src[si] == '\n' || src[si] == '\r')) {
                si++;
            }

            if (in == padChar) {
                switch (j) {
                    case 0:
                    case 1:
                        throw CorruptInputException(si - 1);
                    case 2:
                        if (si == size) {
                            throw CorruptInputException(size);
                        }
                        if (src[si] != padChar) {
                            throw CorruptInputException(si - 1);
                        }

                        si++;
                        while (si < size && (src[si] == '\n' || src[si] == '\r')) {
                            si++;
                        }
                        break;
                }
                if (si < size) {
                    throw CorruptInputException(si);
                }
                dinc = 3, dlen = j, end = true;
                break;
            }
            dbuf[j] = decodeMap[in];
            if (dbuf[j] == 0xFF) {
                throw CorruptInputException(si - 1);
            }
        }
        uint32_t val = ((uint32_t) dbuf[0]) << 18 | ((uint32_t) dbuf[1]) << 12 |
                       ((uint32_t) dbuf[2]) << 6 | ((uint32_t) dbuf[3]);
        switch (dlen) {
            case 4:
                dst[2] = (uint8_t) (val >> 0);
                /*fallthrough*/
            case 3:
                dst[1] = (uint8_t) (val >> 8);
                /*falthrough*/
            case 2:
                dst[0] = (uint8_t) (val >> 16);
                break;
        }
        dst += dinc;
        n += dlen - 1;
    }

    return n;
}

size_t Base64::DecodeString(uint8_t *dst, const std::string &s) {
    return Decode(dst, (const uint8_t *) s.c_str(), s.size());
}

const std::string Base64::encodeStd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const std::string Base64::encodeURL = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

const uint8_t Base64::StdPadding = '=';
const uint8_t Base64::NoPadding = -1;