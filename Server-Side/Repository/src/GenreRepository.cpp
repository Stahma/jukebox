#include "repository/GenreRepository.h"

GenreRepository::GenreRepository(std::shared_ptr<DBContext> context) : BaseRepository<Genre>(context) { }

GenreRepository::~GenreRepository() { }


std::shared_ptr<Genre> GenreRepository::GetById(uint64_t id) {
    return Find(id);
}

std::shared_ptr<Genre> GenreRepository::FindByName(const std::string &name) {
    return Get(odb::query<Genre>::name.equal(name));
}

std::vector<std::shared_ptr<Genre>> GenreRepository::GetAll() {
    return GetList();
}

bool GenreRepository::SaveEntity(std::shared_ptr<Genre> object) {
    return Save(object);
}

bool GenreRepository::UpdateEntity(std::shared_ptr<Genre> object) {
    return Update(object);
}

bool GenreRepository::DeleteEntity(std::shared_ptr<Genre> object) {
    return Delete(object);
}


