#include "repository/AlbumRepository.h"

AlbumRepository::AlbumRepository(std::shared_ptr<DBContext> context) : BaseRepository<Album>(context) { }

AlbumRepository::~AlbumRepository() { }

std::shared_ptr<Album> AlbumRepository::GetById(uint64_t id) {
    return Find(id);
}

std::vector<std::shared_ptr<Album>> AlbumRepository::FindByName(const std::string &name) {
    return GetList(odb::query<Album>::name.equal(name));
}

std::vector<std::shared_ptr<Album>> AlbumRepository::SearchByName(const std::string &name) {
    return GetList(odb::query<Album>::name.like("%" + name + "%"));
}

std::vector<std::shared_ptr<Album>> AlbumRepository::GetAll() {
    return GetList();
}

bool AlbumRepository::SaveEntity(std::shared_ptr<Album> object) {
    return Save(object);
}

bool AlbumRepository::UpdateEntity(std::shared_ptr<Album> object) {
    return Update(object);
}