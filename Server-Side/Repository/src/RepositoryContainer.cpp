#include "repository/RepositoryContainer.h"

RepositoryContainer::RepositoryContainer() { }

RepositoryContainer::~RepositoryContainer() { }

void RepositoryContainer::Register(std::shared_ptr<IPerformerRepository> repository) {
    performers = repository;
}

void RepositoryContainer::Register(std::shared_ptr<IAlbumRepository> repository) {
    albums = repository;
}

void RepositoryContainer::Register(std::shared_ptr<ITrackRepository> repository) {
    tracks = repository;
}

void RepositoryContainer::Register(std::shared_ptr<IGenreRepository> repository) {
    genres = repository;
}

void RepositoryContainer::Register(std::shared_ptr<ILyricsRepository> repository) {
    lyrics = repository;
}


std::shared_ptr<IPerformerRepository> RepositoryContainer::PerformerRepository() {
    return performers;
}

std::shared_ptr<IAlbumRepository> RepositoryContainer::AlbumRepository() {
    return albums;
}

std::shared_ptr<ITrackRepository> RepositoryContainer::TrackRepository() {
    return tracks;
}

std::shared_ptr<IGenreRepository> RepositoryContainer::GenreRepository() {
    return genres;
}

std::shared_ptr<ILyricsRepository> RepositoryContainer::LyricsRepository() {
    return lyrics;
}

