#include "repository/TrackRepository.h"

TrackRepository::TrackRepository(std::shared_ptr<DBContext> context) : BaseRepository<Track>(context) { }

TrackRepository::~TrackRepository() { }

std::shared_ptr<Track> TrackRepository::GetById(uint64_t id) {
    return Find(id);
}

std::vector<std::shared_ptr<Track>> TrackRepository::FindByName(const std::string &name) {
    return GetList(odb::query<Track>::name.equal(name));
}

std::vector<std::shared_ptr<Track>> TrackRepository::SearchByName(const std::string &name) {
    return GetList(odb::query<Track>::name.like("%" + name + "%"));
}

std::vector<std::shared_ptr<Track>> TrackRepository::GetAll() {
    return GetList();
}

bool TrackRepository::SaveEntity(std::shared_ptr<Track> object) {
    return Save(object);
}

bool TrackRepository::UpdateEntity(std::shared_ptr<Track> object) {
    return Update(object);
}

