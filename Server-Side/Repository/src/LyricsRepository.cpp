#include "repository/LyricsRepository.h"


LyricsRepository::LyricsRepository(std::shared_ptr<DBContext> context) : BaseRepository<Lyrics>(context) { }

LyricsRepository::~LyricsRepository() { }

std::shared_ptr<Lyrics> LyricsRepository::GetById(uint64_t id) {
    return Find(id);
}

bool LyricsRepository::SaveEntity(std::shared_ptr<Lyrics> object) {
    return Save(object);
}

bool LyricsRepository::UpdateEntity(std::shared_ptr<Lyrics> object) {
    return Update(object);
}

bool LyricsRepository::DeleteEntity(std::shared_ptr<Lyrics> object) {
    return Delete(object);
}