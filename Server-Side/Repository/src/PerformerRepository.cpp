#include "repository/PerformerRepository.h"

PerformerRepository::PerformerRepository(std::shared_ptr<DBContext> context) : BaseRepository<Performer>(context) { }

PerformerRepository::~PerformerRepository() { }

std::shared_ptr<Performer> PerformerRepository::GetById(uint64_t id) {
    return Find(id);
}

std::vector<std::shared_ptr<Performer>> PerformerRepository::FindByName(const std::string &name) {
    return GetList(odb::query<Performer>::name.equal(name));
}

std::vector<std::shared_ptr<Performer>> PerformerRepository::SearchByName(const std::string &name) {
    return GetList(odb::query<Performer>::name.like("%" + name + "%"));
}

std::vector<std::shared_ptr<Performer>> PerformerRepository::GetAll() {
    return GetList();
}

bool PerformerRepository::SaveEntity(std::shared_ptr<Performer> object) {
    return Save(object);
}

bool PerformerRepository::UpdateEntity(std::shared_ptr<Performer> object) {
    return Update(object);
}