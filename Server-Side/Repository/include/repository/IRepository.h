#pragma once

#include <memory>
#include <vector>
#include <odb/query.hxx>
#include <odb/lazy-ptr.hxx>

template<class T>
class IRepository {
public:
    IRepository() { }

    virtual ~IRepository() { }

    virtual std::shared_ptr<T> Find(uint64_t id) = 0;

    virtual bool Save(std::shared_ptr<T> object) = 0;

    virtual bool Update(std::shared_ptr<T> object) = 0;

    virtual bool Delete(std::shared_ptr<T> object) = 0;

    virtual std::shared_ptr<T> Get(const odb::query<T> &predicate) = 0;

    virtual std::vector<std::shared_ptr<T>> GetList() = 0;

    virtual std::vector<std::shared_ptr<T>> GetList(const odb::query<T> &predicate) = 0;
};
