#pragma once

#include <database/Models.h>

class IGenreRepository {
public:
    IGenreRepository() { }

    virtual ~IGenreRepository() { }

    virtual std::shared_ptr<Genre> GetById(uint64_t id) = 0;

    virtual std::shared_ptr<Genre> FindByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Genre>> GetAll() = 0;

    virtual bool SaveEntity(std::shared_ptr<Genre> object) = 0;

    virtual bool UpdateEntity(std::shared_ptr<Genre> object) = 0;

    virtual bool DeleteEntity(std::shared_ptr<Genre> object) = 0;
};
