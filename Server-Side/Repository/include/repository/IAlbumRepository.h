#pragma once

#include <database/Models.h>

class IAlbumRepository {
public:
    IAlbumRepository() { }

    virtual ~IAlbumRepository() { }

    virtual std::shared_ptr<Album> GetById(uint64_t id) = 0;

    virtual std::vector<std::shared_ptr<Album>> FindByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Album>> SearchByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Album>> GetAll() = 0;

    virtual bool SaveEntity(std::shared_ptr<Album> object) = 0;

    virtual bool UpdateEntity(std::shared_ptr<Album> object) = 0;
};
