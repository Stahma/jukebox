#pragma once

#include "ILyricsRepository.h"
#include "BaseRepository.h"

class LyricsRepository : public BaseRepository<Lyrics>, public ILyricsRepository {
public:
    LyricsRepository(std::shared_ptr<DBContext> context);

    virtual ~LyricsRepository();

    virtual std::shared_ptr<Lyrics> GetById(uint64_t id) override;

    virtual bool SaveEntity(std::shared_ptr<Lyrics> object) override;

    virtual bool UpdateEntity(std::shared_ptr<Lyrics> object) override;

    virtual bool DeleteEntity(std::shared_ptr<Lyrics> object) override;
};
