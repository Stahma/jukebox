#pragma once

#include <database/Models.h>

class ITrackRepository {
public:
    ITrackRepository() { }

    virtual ~ITrackRepository() { }

    virtual std::shared_ptr<Track> GetById(uint64_t id) = 0;

    virtual std::vector<std::shared_ptr<Track>> FindByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Track>> SearchByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Track>> GetAll() = 0;

    virtual bool SaveEntity(std::shared_ptr<Track> object) = 0;

    virtual bool UpdateEntity(std::shared_ptr<Track> object) = 0;
};
