#pragma once

#include "ILyricsRepository.h"
#include "IGenreRepository.h"
#include "ITrackRepository.h"
#include "IAlbumRepository.h"
#include "IPerformerRepository.h"

class RepositoryContainer {
public:
    RepositoryContainer();

    virtual ~RepositoryContainer();

    void Register(std::shared_ptr<IPerformerRepository> repository);

    void Register(std::shared_ptr<IAlbumRepository> repository);

    void Register(std::shared_ptr<ITrackRepository> repository);

    void Register(std::shared_ptr<IGenreRepository> repository);

    void Register(std::shared_ptr<ILyricsRepository> repository);

    std::shared_ptr<IPerformerRepository> PerformerRepository();

    std::shared_ptr<IAlbumRepository> AlbumRepository();

    std::shared_ptr<ITrackRepository> TrackRepository();

    std::shared_ptr<IGenreRepository> GenreRepository();

    std::shared_ptr<ILyricsRepository> LyricsRepository();

private:
    std::shared_ptr<IPerformerRepository> performers;
    std::shared_ptr<IAlbumRepository> albums;
    std::shared_ptr<ITrackRepository> tracks;
    std::shared_ptr<IGenreRepository> genres;
    std::shared_ptr<ILyricsRepository> lyrics;
};
