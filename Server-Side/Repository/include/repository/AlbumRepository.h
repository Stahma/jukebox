#pragma once

#include "BaseRepository.h"
#include "IAlbumRepository.h"

class AlbumRepository : public BaseRepository<Album>, public IAlbumRepository {
public:
    AlbumRepository(std::shared_ptr<DBContext> context);

    virtual ~AlbumRepository();

    virtual std::shared_ptr<Album> GetById(uint64_t id) override;

    virtual std::vector<std::shared_ptr<Album>> FindByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Album>> SearchByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Album>> GetAll() override;

    virtual bool SaveEntity(std::shared_ptr<Album> object) override;

    virtual bool UpdateEntity(std::shared_ptr<Album> object) override;
};
