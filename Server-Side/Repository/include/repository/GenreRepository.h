#pragma once

#include "BaseRepository.h"
#include "IGenreRepository.h"

class GenreRepository : public BaseRepository<Genre>, public IGenreRepository {
public:
    GenreRepository(std::shared_ptr<DBContext> context);

    virtual ~GenreRepository();

    virtual std::shared_ptr<Genre> GetById(uint64_t id) override;

    virtual std::shared_ptr<Genre> FindByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Genre>> GetAll() override;

    virtual bool SaveEntity(std::shared_ptr<Genre> object) override;

    virtual bool UpdateEntity(std::shared_ptr<Genre> object) override;

    virtual bool DeleteEntity(std::shared_ptr<Genre> object) override;
};
