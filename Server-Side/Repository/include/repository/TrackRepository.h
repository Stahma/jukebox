#pragma once

#include "ITrackRepository.h"
#include "BaseRepository.h"

class TrackRepository : public BaseRepository<Track>, public ITrackRepository {
public:
    TrackRepository(std::shared_ptr<DBContext> context);

    virtual ~TrackRepository();

    virtual std::shared_ptr<Track> GetById(uint64_t id) override;

    virtual std::vector<std::shared_ptr<Track>> FindByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Track>> SearchByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Track>> GetAll() override;

    virtual bool SaveEntity(std::shared_ptr<Track> object) override;

    virtual bool UpdateEntity(std::shared_ptr<Track> object) override;
};
