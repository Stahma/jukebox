#pragma once

#include "repository/BaseRepository.h"
#include "IPerformerRepository.h"

class PerformerRepository : public BaseRepository<Performer>, public IPerformerRepository {
public:
    PerformerRepository(std::shared_ptr<DBContext> context);

    virtual ~PerformerRepository();

    virtual std::shared_ptr<Performer> GetById(uint64_t id) override;

    virtual std::vector<std::shared_ptr<Performer>> FindByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Performer>> SearchByName(const std::string &name) override;

    virtual std::vector<std::shared_ptr<Performer>> GetAll() override;

    virtual bool SaveEntity(std::shared_ptr<Performer> object) override;

    virtual bool UpdateEntity(std::shared_ptr<Performer> object) override;
};
