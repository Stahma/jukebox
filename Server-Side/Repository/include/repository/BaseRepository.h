#pragma once

#include "IRepository.h"
#include <database/DBContext.h>

template<class T>
class BaseRepository : public IRepository<T> {
public:
    BaseRepository(std::shared_ptr<DBContext> context);

    virtual ~BaseRepository();

    virtual std::shared_ptr<T> Find(uint64_t id) override;

    virtual bool Save(std::shared_ptr<T> object) override;

    virtual bool Update(std::shared_ptr<T> object) override;

    virtual bool Delete(std::shared_ptr<T> object) override;

    virtual std::shared_ptr<T> Get(const odb::query<T> &predicate) override;

    virtual std::vector<std::shared_ptr<T>> GetList() override;

    virtual std::vector<std::shared_ptr<T>> GetList(const odb::query<T> &predicate) override;

private:
    std::shared_ptr<DBContext> context;
};

template<class T>
BaseRepository<T>::BaseRepository(std::shared_ptr<DBContext> context)
        : IRepository<T>(), context(std::move(context)) { }

template<class T>
BaseRepository<T>::~BaseRepository() { }

template<class T>
std::shared_ptr<T> BaseRepository<T>::Find(uint64_t id) {
    try {
        return context->Find<T>(id);
    } catch (const odb::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return nullptr;
}

template<class T>
bool BaseRepository<T>::Save(std::shared_ptr<T> object) {
    try {
        return context->Save<T>(object);
    } catch (const odb::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return false;
}

template<class T>
bool BaseRepository<T>::Update(std::shared_ptr<T> object) {
    try {
        return context->Update<T>(object);
    } catch (const odb::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return false;
}

template<class T>
bool BaseRepository<T>::Delete(std::shared_ptr<T> object) {
    try {
        return context->Delete<T>(object->Id());
    } catch (const odb::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return false;
}

template<class T>
std::shared_ptr<T> BaseRepository<T>::Get(const odb::query<T> &predicate) {
    try {
        return context->QueryOne<T>(predicate);
    } catch (const odb::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return nullptr;
}

template<class T>
std::vector<std::shared_ptr<T>> BaseRepository<T>::GetList() {
    return GetList(odb::query<T>());
}

template<class T>
std::vector<std::shared_ptr<T>> BaseRepository<T>::GetList(const odb::query<T> &predicate) {
    try {
        return context->Query<T>(predicate);
    } catch (const odb::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return { };
}











