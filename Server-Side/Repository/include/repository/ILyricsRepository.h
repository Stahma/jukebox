#pragma once

#include <database/Models.h>

class ILyricsRepository {
public:
    ILyricsRepository() { }

    virtual ~ILyricsRepository() { }

    virtual std::shared_ptr<Lyrics> GetById(uint64_t id) = 0;

    virtual bool SaveEntity(std::shared_ptr<Lyrics> object) = 0;

    virtual bool UpdateEntity(std::shared_ptr<Lyrics> object) = 0;

    virtual bool DeleteEntity(std::shared_ptr<Lyrics> object) = 0;
};
