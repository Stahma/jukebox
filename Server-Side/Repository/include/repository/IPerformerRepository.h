#pragma once

#include <database/Models.h>

class IPerformerRepository {
public:
    IPerformerRepository() { }

    virtual ~IPerformerRepository() { }

    virtual std::shared_ptr<Performer> GetById(uint64_t id) = 0;

    virtual std::vector<std::shared_ptr<Performer>> FindByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Performer>> SearchByName(const std::string &name) = 0;

    virtual std::vector<std::shared_ptr<Performer>> GetAll() = 0;

    virtual bool SaveEntity(std::shared_ptr<Performer> object) = 0;

    virtual bool UpdateEntity(std::shared_ptr<Performer> object) = 0;
};
