#pragma once

#include <string>
#include <functional>
#include <memory>

class ICommandManager {
public:
    ICommandManager() { }

    virtual ~ICommandManager() { }

    virtual std::function<std::string()> GetSuitableFunction(const std::string &request) = 0;
};
