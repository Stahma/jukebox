#pragma once

#include <string>
#include <functional>
#include <dbservice/IDatabaseService.h>
#include <storage/IStorageService.h>
#include <unordered_map>
#include "ICommandManager.h"

class CommandManager : public ICommandManager {
public:
    CommandManager(std::shared_ptr<IDatabaseService> databaseService, std::shared_ptr<IStorageService> storageService);

    virtual ~CommandManager();

    virtual std::function<std::string()> GetSuitableFunction(const std::string &request);

private:
    std::shared_ptr<IDatabaseService> databaseService;
    std::shared_ptr<IStorageService> storageService;
    std::unordered_map<std::string, std::function<std::string(std::string)>> commands;
};
