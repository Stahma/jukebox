#define RAPIDJSON_HAS_STDSTRING 1

#include <rapidjson/document.h>
#include <iostream>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include "cmanager/CommandManager.h"

CommandManager::CommandManager(std::shared_ptr<IDatabaseService> databaseService,
                               std::shared_ptr<IStorageService> storageService) {

    this->databaseService = databaseService;
    this->storageService = storageService;

    using std::placeholders::_1;

    commands["/get/performer/"] = std::bind(&IDatabaseService::GetPerformer, databaseService, _1);
    commands["/get/album/"] = std::bind(&IDatabaseService::GetAlbum, databaseService, _1);
    commands["/get/track/"] = std::bind(&IDatabaseService::GetTrack, databaseService, _1);
    commands["/get/genre/"] = std::bind(&IDatabaseService::GetGenre, databaseService, _1);
    commands["/get/lyrics/"] = std::bind(&IDatabaseService::GetLyrics, databaseService, _1);
    commands["/get/link/"] = std::bind(&IStorageService::GetLink, storageService, _1);
    commands["/search/performer/"] = std::bind(&IDatabaseService::SearchPerformer, databaseService, _1);
    commands["/search/album/"] = std::bind(&IDatabaseService::SearchAlbum, databaseService, _1);
    commands["/search/track/"] = std::bind(&IDatabaseService::SearchTrack, databaseService, _1);
    commands["/search/all/"] = std::bind(&IDatabaseService::SearchAll, databaseService, _1);
    commands["/find/performer/"] = std::bind(&IDatabaseService::FindPerformer, databaseService, _1);
    commands["/find/album/"] = std::bind(&IDatabaseService::FindAlbum, databaseService, _1);
    commands["/find/track/"] = std::bind(&IDatabaseService::FindTrack, databaseService, _1);
    commands["/find/genre/"] = std::bind(&IDatabaseService::FindGenre, databaseService, _1);
    commands["/all/performer/"] = std::bind(&IDatabaseService::AllPerformer, databaseService, _1);
    commands["/all/album/"] = std::bind(&IDatabaseService::AllAlbum, databaseService, _1);
    commands["/all/track/"] = std::bind(&IDatabaseService::AllTrack, databaseService, _1);
    commands["/all/genre/"] = std::bind(&IDatabaseService::AllGenre, databaseService, _1);
    commands["/genre/performer/"] = std::bind(&IDatabaseService::GenrePerformer, databaseService, _1);
    commands["/genre/album/"] = std::bind(&IDatabaseService::GenreAlbum, databaseService, _1);
    commands["/genre/track/"] = std::bind(&IDatabaseService::GenreTrack, databaseService, _1);
    commands["/genre/all/"] = std::bind(&IDatabaseService::GenreAll, databaseService, _1);
    commands["/write/performer/"] = std::bind(&IDatabaseService::WritePerformer, databaseService, _1);
    commands["/write/album/"] = std::bind(&IDatabaseService::WriteAlbum, databaseService, _1);
    commands["/write/track/"] = std::bind(&IDatabaseService::WriteTrack, databaseService, _1);
    commands["/write/genre/"] = std::bind(&IDatabaseService::WriteGenre, databaseService, _1);
    commands["/write/lyrics/"] = std::bind(&IDatabaseService::WriteLyrics, databaseService, _1);
    commands["/write/link/"] = std::bind(&IStorageService::WriteLink, storageService, _1);
}

CommandManager::~CommandManager() { }

std::function<std::string()> CommandManager::GetSuitableFunction(const std::string &request) {
    rapidjson::Document document;
    document.Parse(request);

    if (document.HasParseError()) {
        std::cerr << "Error: request parsing error at " << document.GetErrorOffset() << std::endl;
        throw std::runtime_error("Error: invalid request.");
    }

    rapidjson::Value::ConstMemberIterator command = document.FindMember("Command");
    if (command == document.MemberEnd()) {
        std::cerr << "Error: command field not found." << std::endl;
        throw std::runtime_error("Invalid request.");
    }

    if (commands.count(command->value.GetString()) == 0) {
        std::cerr << "Error: unknown command." << std::endl;
        throw std::runtime_error("Invalid request.");
    }

    std::function<std::string(std::string)> &f = commands[command->value.GetString()];

    rapidjson::Value::ConstMemberIterator data = document.FindMember("Data");
    if (data == document.MemberEnd()) {
        std::cerr << "Error: data field not found." << std::endl;
        throw std::runtime_error("Invalid request.");
    }

    if (!data->value.IsObject()) {
        std::cerr << "Error: data not JSON object." << std::endl;
        throw std::runtime_error("Invalid request.");
    }

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    data->value.Accept(writer);

    std::string dataAsString = buffer.GetString();

    return std::bind(f, std::move(dataAsString));
}





