﻿#include <stdint.h>
#include <database/Models.h>
#include <database/Models-odb.hxx>

/*Performer*/
Performer::Performer() : Performer(std::string(), std::string()) { }

Performer::Performer(const std::string &name, const std::string &description,
                     const std::string &photo/* = "/default.jpeg"*/) :
        name(name), description(description), photo(photo) { }

Performer::~Performer() { }

uint64_t Performer::Id() const {
    return id;
}

const std::string &Performer::Name() const {
    return name;
}

const std::string &Performer::Description() const {
    return description;
}

const std::string &Performer::Photo() const {
    return photo;
}

const std::vector<std::weak_ptr<Album>> &Performer::Albums() const {
    return albums;
}

const std::vector<std::weak_ptr<Track>> &Performer::Tracks() const {
    return tracks;
}

const std::vector<std::shared_ptr<Genre>> &Performer::Genres() const {
    return genres;
}

std::shared_ptr<Performer> Performer::Deserialize(const rapidjson::Value &value) {
    std::shared_ptr<Performer> object(new Performer());

    if (value.HasMember("Id")) {
        object->id = value["Id"].GetUint64();
        return object;
    }

    if (!value.HasMember("Name")) {
        std::cerr << "Error: name is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    if (!value.HasMember("Description")) {
        std::cerr << "Error: description is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    object->name = value["Name"].GetString();
    object->description = value["Description"].GetString();

    object->photo = "/default.jpeg";
    if (value.HasMember("Photo")) {
        object->photo = value["Photo"].GetString();
    }

    if (value.HasMember("Genres")) {
        for (const rapidjson::Value &g : value["Genres"].GetArray()) {
            std::shared_ptr<Genre> genre = Genre::Deserialize(g);
            object->genres.emplace_back(genre);
            genre->performers.emplace_back(object);
        }
    }

    return object;
}

/*Album*/
Album::Album() : Album(std::string(), boost::gregorian::date()) { }

Album::Album(const std::string &name, const boost::gregorian::date &date,
             const std::string &photo/* = "/default.jpeg"*/,
             std::shared_ptr<std::string> label/* = nullptr*/) :
        name(name), date(date), photo(photo), label(label) { }

Album::~Album() { }

uint64_t Album::Id() const {
    return id;
}

const std::string &Album::Name() const {
    return name;
}

const boost::gregorian::date &Album::Date() const {
    return date;
}

const std::string &Album::Photo() const {
    return photo;
}

std::shared_ptr<std::string> Album::Label() const {
    return label;
}

std::shared_ptr<Performer> Album::Performer() const {
    return performer;
}

const std::vector<std::weak_ptr<Track>> &Album::Tracks() const {
    return tracks;
}

const std::vector<std::shared_ptr<Genre>> &Album::Genres() const {
    return genres;
}

std::shared_ptr<Album> Album::Deserialize(const rapidjson::Value &value) {
    std::shared_ptr<Album> object(new Album());

    if (value.HasMember("Id")) {
        object->id = value["Id"].GetUint64();
        return object;
    }

    if (!value.HasMember("Name")) {
        std::cerr << "Error: name is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    if (!value.HasMember("Date")) {
        std::cerr << "Error: date is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    if (!value.HasMember("Performer")) {
        std::cerr << "Error: performer is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    object->name = value["Name"].GetString();
    object->date = boost::gregorian::from_simple_string(value["Date"].GetString());

    object->performer = Performer::Deserialize(value["Performer"]);

    object->photo = "/default.jpeg";
    if (value.HasMember("Photo")) {
        object->photo = value["Photo"].GetString();
    }

    if (value.HasMember("Label")) {
        object->label = std::make_shared<std::string>(value["Label"].GetString());
    }

    if (value.HasMember("Genres")) {
        for (const rapidjson::Value &g : value["Genres"].GetArray()) {
            std::shared_ptr<Genre> genre = Genre::Deserialize(g);
            object->genres.emplace_back(genre);
            genre->albums.emplace_back(object);
        }
    }

    return object;
}

/*Track*/
Track::Track() : Track(std::string()) { }

Track::Track(const std::string &name,
             std::shared_ptr<::Lyrics> lyrics/* = nullptr*/) :
        name(name), lyrics(lyrics) { }

Track::~Track() { }

uint64_t Track::Id() const {
    return id;
}

const std::string &Track::Name() const {
    return name;
}

const std::string &Track::Key() const {
    return key;
}

std::shared_ptr<::Lyrics> Track::Lyrics() const {
    return lyrics;
}

std::shared_ptr<Performer> Track::Performer() const {
    return performer;
}

std::shared_ptr<Album> Track::Album() const {
    return album;
}

const std::vector<std::shared_ptr<Genre>> &Track::Genres() const {
    return genres;
}

std::shared_ptr<Track> Track::Deserialize(const rapidjson::Value &value) {
    std::shared_ptr<Track> object(new Track());

    if (value.HasMember("Id")) {
        object->id = value["Id"].GetUint64();
        return object;
    }

    if (!value.HasMember("Name")) {
        std::cerr << "Error: name is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    if (!value.HasMember("Key")) {
        std::cerr << "Error: key is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    if (!value.HasMember("Album")) {
        std::cerr << "Error: album is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    object->name = value["Name"].GetString();
    object->key = value["Key"].GetString();

    object->performer = Performer::Deserialize(value["Performer"]);
    object->album = Album::Deserialize(value["Album"]);

    if (value.HasMember("Lyrics")) {
        object->lyrics = Lyrics::Deserialize(value["Lyrics"]);
    }

    if (value.HasMember("Genres")) {
        for (const rapidjson::Value &g : value["Genres"].GetArray()) {
            std::shared_ptr<Genre> genre = Genre::Deserialize(g);
            object->genres.emplace_back(genre);
            genre->tracks.emplace_back(object);
        }
    }

    return object;
}

/*Genre*/
Genre::Genre() : Genre(std::string()) { }

Genre::Genre(const std::string &name) : name(name) { }

Genre::~Genre() { }

uint64_t Genre::Id() const {
    return id;
}

const std::string &Genre::Name() const {
    return name;
}

const std::vector<std::weak_ptr<Performer>> &Genre::Performers() const {
    return performers;
}

const std::vector<std::weak_ptr<Album>> &Genre::Albums() const {
    return albums;
}

const std::vector<std::weak_ptr<Track>> &Genre::Tracks() const {
    return tracks;
}

std::shared_ptr<Genre> Genre::Deserialize(const rapidjson::Value &value) {
    std::shared_ptr<Genre> object(new Genre());

    if (value.HasMember("Id")) {
        object->id = value["Id"].GetUint64();
        return object;
    }

    if (!value.HasMember("Name")) {
        std::cerr << "Error: name is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    object->name = value["Name"].GetString();

    return object;
}

/*Lyrics*/
Lyrics::Lyrics() : track(), text() { }

Lyrics::Lyrics(std::weak_ptr<::Track> track, const std::string &text) : track(track), text(text) { }

Lyrics::~Lyrics() { }

uint64_t Lyrics::Id() const {
    return id;
}

std::weak_ptr<::Track> Lyrics::Track() const {
    return track;
}

const std::string Lyrics::Text() const {
    return text;
}

void Lyrics::Text(const std::string &text) {
    this->text = text;
}

std::shared_ptr<Lyrics> Lyrics::Deserialize(const rapidjson::Value &value) {
    std::shared_ptr<Lyrics> object(new Lyrics());

    if (value.HasMember("Id")) {
        object->id = value["Id"].GetUint64();
    }

    if (!value.HasMember("Track")) {
        std::cerr << "Error: track is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    if (!value.HasMember("Text")) {
        std::cerr << "Error: text is required." << std::endl;
        throw std::runtime_error("Invalid input data.");
    }

    object->track = Track::Deserialize(value["Track"]);
    object->track.lock()->lyrics = object;

    object->text = value["Text"].GetString();

    return object;
}
