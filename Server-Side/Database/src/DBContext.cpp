#include "database/DBContext.h"

DBContext::DBContext(const std::string &user, const std::string &password, const std::string &database) :
        session(false) {
    this->database = CreateDatabase(user, password, database);
}

DBContext::~DBContext() { }