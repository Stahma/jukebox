#pragma once

#define DATABASE_MYSQL

#include <string>
#include <memory>
#include <cstdlib>
#include <iostream>

#include <odb/database.hxx>

#if defined(DATABASE_MYSQL)

#include <odb/mysql/database.hxx>

#elif defined(DATABASE_SQLITE)
#include <odb/connection.hxx>
#include <odb/transaction.hxx>
#include <odb/schema-catalog.hxx>
#include <odb/sqlite/database.hxx>
#elif defined(DATABASE_PGSQL)
#include <odb/pgsql/database.hxx>
#elif defined(DATABASE_ORACLE)
#include <odb/oracle/database.hxx>
#elif defined(DATABASE_MSSQL)
#include <odb/mssql/database.hxx>
#else
#error Unknown database; Did you forget to define the DATABASE_* macros?
#endif

inline std::unique_ptr<odb::database> CreateDatabase(
        const std::string &user,
        const std::string &password,
        const std::string &database) {
#if defined(DATABASE_MYSQL)
    std::unique_ptr<odb::database> db(new odb::mysql::database(user, password, database));
#elif defined(DATABASE_SQLITE)
    std::unique_ptr<odb::database> db(new odb::sqlite::database(argc, argv, false, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE));

    {
        connection_ptr c(db->connection());

        c->execute("PRAGMA foreign_keys=OFF");

        transaction t(c->begin());
        schema_catalog::create_schema(*db);
        t.commit();

        c->execute("PRAGMA foreign_keys=ON");
    }
#elif defined(DATABASE_PGSQL)
    std::unique_ptr<odb::database> db(new odb::pgsql::database(argc, argv));
#elif defined(DATABASE_ORACLE)
    std::unique_ptr<odb::database> db(new odb::oracle::database(argc, argv));
#elif defined(DATABASE_MSSQL)
    std::unique_ptr<odb::database> db(new odb::mssql::database(argc, argv));
#endif

    return db;
}

