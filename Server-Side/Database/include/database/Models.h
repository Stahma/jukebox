﻿#pragma once

#define RAPIDJSON_HAS_STDSTRING 1

#include <string>
#include <memory>
#include <vector>
#include <exception>

#include <odb/core.hxx>
#include <odb/vector.hxx>
#include <odb/lazy-ptr.hxx>

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/gregorian/parsers.hpp>
#include <rapidjson/document.h>

class Executor;

class Performer;

class Album;

class Track;

class Genre;

class Lyrics;

/*Performer*/
#pragma db object pointer(std::shared_ptr) session

class Performer : public std::enable_shared_from_this<Performer> {
public:
    enum SP {
        ID = (1 << 0), NAME = (1 << 1), DESCRIPTION = (1 << 2), PHOTO = (1 << 3),
        ALBUMS = (1 << 4), TRACKS = (1 << 5), GENRES = (1 << 6),
        MIN = (ID | NAME), BASIC = (MIN | PHOTO | GENRES), ALL = (BASIC | DESCRIPTION | ALBUMS | TRACKS)
    };

    Performer();

    Performer(const std::string &name, const std::string &description, const std::string &photo = "/default.jpeg");

    virtual ~Performer();

    uint64_t Id() const;

    const std::string &Name() const;

    const std::string &Description() const;

    const std::string &Photo() const;

    const std::vector<std::weak_ptr<Album>> &Albums() const;

    const std::vector<std::weak_ptr<Track>> &Tracks() const;

    const std::vector<std::shared_ptr<Genre>> &Genres() const;

    template<typename Writer>
    void Serialize(Writer &writer, int sp);

    static std::shared_ptr<Performer> Deserialize(const rapidjson::Value &value);

private:
    friend class odb::access;

    friend class Album;

    friend class Track;

    friend class Genre;

#pragma db id auto
    uint64_t id;

    std::string name;

#pragma db type("VARCHAR(512)")
    std::string description;

    std::string photo;

#pragma db inverse(performer)
    std::vector<std::weak_ptr<Album>> albums;

#pragma db inverse(performer)
    std::vector<std::weak_ptr<Track>> tracks;

    std::vector<std::shared_ptr<Genre>> genres;
};

/*Album*/
#pragma db object pointer(std::shared_ptr) session

class Album : public std::enable_shared_from_this<Album> {
public:
    enum SP {
        ID = (1 << 0), NAME = (1 << 1), DATE = (1 << 2), PHOTO = (1 << 3),
        LABEL = (1 << 4), PERFORMER = (1 << 5), TRACKS = (1 << 6), GENRES = (1 << 7),
        MIN = (ID | NAME), BASIC = (MIN | DATE | PHOTO | GENRES | PERFORMER),
        ALL = (BASIC | LABEL | TRACKS)
    };

    Album();

    Album(const std::string &name, const boost::gregorian::date &date,
          const std::string &photo = "/default.jpeg", std::shared_ptr<std::string> label = nullptr);

    virtual ~Album();

    uint64_t Id() const;

    const std::string &Name() const;

    const boost::gregorian::date &Date() const;

    const std::string &Photo() const;

    std::shared_ptr<std::string> Label() const;

    std::shared_ptr<::Performer> Performer() const;

    const std::vector<std::weak_ptr<Track>> &Tracks() const;

    const std::vector<std::shared_ptr<Genre>> &Genres() const;

    template<typename Writer>
    void Serialize(Writer &writer, int sp);

    static std::shared_ptr<Album> Deserialize(const rapidjson::Value &value);

private:
    friend class odb::access;

    friend class Performer;

    friend class Track;

    friend class Genre;

#pragma db id auto
    uint64_t id;

    std::string name;

#pragma db not_null
    boost::gregorian::date date;

    std::string photo;

#pragma db null
    std::shared_ptr<std::string> label;

#pragma db not_null
    std::shared_ptr<::Performer> performer;

#pragma db inverse(album)
    std::vector<std::weak_ptr<Track>> tracks;

    std::vector<std::shared_ptr<Genre>> genres;
};

/*Track*/
#pragma db object pointer(std::shared_ptr) session

class Track : public std::enable_shared_from_this<Track> {
public:
    enum SP {
        ID = (1 << 0), NAME = (1 << 1), KEY = (1 << 3), LYRICS = (1 << 4),
        PERFORMER = (1 << 5), ALBUM = (1 << 6), GENRES = (1 << 7),
        MIN = (ID | NAME), BASIC = (MIN | KEY | ALBUM | PERFORMER | LYRICS),
        ALL = (BASIC | GENRES)
    };

    Track();

    Track(const std::string &name, std::shared_ptr<Lyrics> lyrics = nullptr);

    virtual ~Track();

    uint64_t Id() const;

    const std::string &Name() const;

    const std::string &Key() const;

    std::shared_ptr<::Lyrics> Lyrics() const;

    std::shared_ptr<::Performer> Performer() const;

    std::shared_ptr<::Album> Album() const;

    const std::vector<std::shared_ptr<Genre>> &Genres() const;

    template<typename Writer>
    void Serialize(Writer &writer, int sp);

    static std::shared_ptr<Track> Deserialize(const rapidjson::Value &value);

private:
    friend class odb::access;

    friend class Performer;

    friend class Album;

    friend class Genre;

    friend class Lyrics;

#pragma db id auto
    uint64_t id;

    std::string name;
    std::string key;

#pragma db null
    std::shared_ptr<::Lyrics> lyrics;

#pragma db not_null
    std::shared_ptr<::Performer> performer;

#pragma db not_null
    std::shared_ptr<::Album> album;

    std::vector<std::shared_ptr<Genre>> genres;
};

/*Genre*/
#pragma db object pointer(std::shared_ptr) session

class Genre : public std::enable_shared_from_this<Genre> {
public:
    enum SP {
        ID = (1 << 0), NAME = (1 << 1), PERFORMERS = (1 << 2),
        ALBUMS = (1 << 3), TRACKS = (1 << 4), MIN = (ID | NAME),
        BASIC = MIN, ALL = (MIN | PERFORMERS | ALBUMS | TRACKS)
    };

    Genre();

    Genre(const std::string &name);

    virtual ~Genre();

    uint64_t Id() const;

    const std::string &Name() const;

    const std::vector<std::weak_ptr<Performer>> &Performers() const;

    const std::vector<std::weak_ptr<Album>> &Albums() const;

    const std::vector<std::weak_ptr<Track>> &Tracks() const;

    template<typename Writer>
    void Serialize(Writer &writer, int sp);

    static std::shared_ptr<Genre> Deserialize(const rapidjson::Value &value);

private:
    friend class odb::access;

    friend class Performer;

    friend class Album;

    friend class Track;

#pragma db id auto
    uint64_t id;

#pragma db unique
    std::string name;

#pragma db inverse(genres)
    std::vector<std::weak_ptr<Performer>> performers;

#pragma db inverse(genres)
    std::vector<std::weak_ptr<Album>> albums;

#pragma db inverse(genres)
    std::vector<std::weak_ptr<Track>> tracks;
};

/*Lyrics*/
#pragma db object pointer(std::shared_ptr) session

class Lyrics : public std::enable_shared_from_this<Lyrics> {
public:
    enum SP {
        ID = (1 << 0), TRACK = (1 << 1), TEXT = (1 << 2),
        MIN = (ID), BASIC = (MIN | TEXT), ALL = BASIC
    };

    Lyrics();

    Lyrics(std::weak_ptr<::Track> track, const std::string &text);

    virtual ~Lyrics();

    uint64_t Id() const;

    std::weak_ptr<::Track> Track() const;

    const std::string Text() const;

    void Text(const std::string &text);

    template<typename Writer>
    void Serialize(Writer &writer, int sp);

    static std::shared_ptr<Lyrics> Deserialize(const rapidjson::Value &value);

private:
    friend class odb::access;

    friend class Track;

#pragma db id auto
    uint64_t id;

#pragma db inverse(lyrics) not_null
    std::weak_ptr<::Track> track;

#pragma db type("VARCHAR(2048)")
    std::string text;
};

#pragma db value(std::string) type("VARCHAR(128)")

/*Don't move methods below to .cpp file. It won't link.*/

#ifndef ODB_COMPILER

#define ODB_CXX11_NULLPTR

template<typename Writer>
void Performer::Serialize(Writer &writer, int sp) {
    writer.StartObject();
    {
        if (sp & ID) {
            writer.Key("Id");
            writer.Uint64(id);
        }

        if (sp & NAME) {
            writer.Key("Name");
            writer.String(name);
        }

        if (sp & PHOTO) {
            writer.Key("Photo");
            writer.String(photo);
        }

        if (sp & DESCRIPTION) {
            writer.Key("Description");
            writer.String(description);
        }

        if (sp & ALBUMS) {
            writer.Key("Albums");
            writer.StartArray();
            {
                for (std::weak_ptr<Album> p : albums) {
                    p.lock()->Serialize(writer, Album::BASIC);
                }
            }
            writer.EndArray();
        }

        if (sp & TRACKS) {
            writer.Key("Tracks");
            writer.StartArray();
            {
                for (std::weak_ptr<Track> p : tracks) {
                    p.lock()->Serialize(writer, Track::BASIC);
                }
            }
            writer.EndArray();
        }

        if (sp & GENRES) {
            writer.Key("Genres");
            writer.StartArray();
            {
                for (std::shared_ptr<Genre> p : genres) {
                    p->Serialize(writer, Genre::MIN);
                }
            }
            writer.EndArray();
        }
    }
    writer.EndObject();
}

template<typename Writer>
void Album::Serialize(Writer &writer, int sp) {
    writer.StartObject();
    {
        if (sp & ID) {
            writer.Key("Id");
            writer.Uint64(id);
        }

        if (sp & NAME) {
            writer.Key("Name");
            writer.String(name);
        }

        if (sp & DATE) {
            writer.Key("Date");
            writer.String(boost::gregorian::to_simple_string(date));
        }

        if (sp & PHOTO) {
            writer.Key("Photo");
            writer.String(photo);
        }

        if (sp & PERFORMER) {
            writer.Key("Performer");
            performer->Serialize(writer, Performer::MIN);
        }

        if (sp & LABEL && label != nullptr) {
            writer.Key("Label");
            writer.String(*label);
        }

        if (sp & TRACKS) {
            writer.Key("Tracks");
            writer.StartArray();
            {
                for (std::weak_ptr<Track> p : tracks) {
                    p.lock()->Serialize(writer, Track::BASIC ^ Track::ALBUM);
                }
            }
            writer.EndArray();
        }

        if (sp & GENRES) {
            writer.Key("Genres");
            writer.StartArray();
            {
                for (std::shared_ptr<Genre> p : genres) {
                    p->Serialize(writer, Genre::MIN);
                }
            }
            writer.EndArray();
        }
    }
    writer.EndObject();
}

template<typename Writer>
void Track::Serialize(Writer &writer, int sp) {
    writer.StartObject();
    {
        if (sp & ID) {
            writer.Key("Id");
            writer.Uint64(id);
        }

        if (sp & NAME) {
            writer.Key("Name");
            writer.String(name);
        }

        if (sp & KEY) {
            writer.Key("Key");
            writer.String(key);
        }

        if (sp & PERFORMER) {
            writer.Key("Performer");
            performer->Serialize(writer, Performer::MIN);
        }

        if (sp & ALBUM) {
            writer.Key("Album");
            album->Serialize(writer, Album::MIN | Album::PHOTO);
        }

        if (sp & LYRICS && lyrics != nullptr) {
            writer.Key("Lyrics");
            lyrics->Serialize(writer, Lyrics::MIN);
        }

        if (sp & GENRES) {
            writer.Key("Genres");
            writer.StartArray();
            {
                for (std::shared_ptr<Genre> p : genres) {
                    p->Serialize(writer, Genre::MIN);
                }
            }
            writer.EndArray();
        }
    }
    writer.EndObject();
}

template<typename Writer>
void Genre::Serialize(Writer &writer, int sp) {
    writer.StartObject();
    {
        if (sp & ID) {
            writer.Key("Id");
            writer.Uint64(id);
        }

        if (sp & NAME) {
            writer.Key("Name");
            writer.String(name);
        }

        if (sp & PERFORMERS) {
            writer.Key("Performers");
            writer.StartArray();
            {
                for (std::weak_ptr<Performer> p : performers) {
                    p.lock()->Serialize(writer, Track::BASIC);
                }
            }
            writer.EndArray();
        }

        if (sp & ALBUMS) {
            writer.Key("Albums");
            writer.StartArray();
            {
                for (std::weak_ptr<Album> p : albums) {
                    p.lock()->Serialize(writer, Track::BASIC);
                }
            }
            writer.EndArray();
        }

        if (sp & TRACKS) {
            writer.Key("Tracks");
            writer.StartArray();
            {
                for (std::weak_ptr<Track> p : tracks) {
                    p.lock()->Serialize(writer, Track::BASIC);
                }
            }
            writer.EndArray();
        }
    }
    writer.EndObject();
}

template<typename Writer>
void Lyrics::Serialize(Writer &writer, int sp) {
    writer.StartObject();
    {
        if (sp & ID) {
            writer.Key("Id");
            writer.Uint64(id);
        }

        if (sp & TEXT) {
            writer.Key("Text");
            writer.String(text);
        }
    }
    writer.EndObject();
}

#endif
