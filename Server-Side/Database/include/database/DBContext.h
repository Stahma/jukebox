#pragma once

#include <string>
#include <memory>
#include <odb/lazy-ptr.hxx>
#include <odb/mysql/query.hxx>
#include <odb/session.hxx>
#include "Database.h"
#include "Models-odb.hxx"

class DBContext {
public:
    DBContext(const std::string &user, const std::string &password, const std::string &database);

    virtual ~DBContext();

    template<class T>
    inline std::shared_ptr<T> Find(uint64_t id);

    template<class T>
    inline bool Save(std::shared_ptr<T> object);

    template<typename T>
    inline bool Update(std::shared_ptr<T> object);

    template<typename T>
    inline bool Delete(uint64_t id);

    template<typename T>
    inline std::shared_ptr<T> QueryOne(const odb::query<T> &query);

    template<typename T>
    inline std::vector<std::shared_ptr<T>> Query(const odb::query<T> &query);

private:
    std::unique_ptr<odb::database> database;
    odb::session session;
};

template<class T>
std::shared_ptr<T> DBContext::Find(uint64_t id) {
    odb::session::current(session);

    std::shared_ptr<T> result = nullptr;

    odb::core::transaction transaction(database->begin());
    {
        result = database->find<T>(id);
    }
    transaction.commit();

    return result;
}

template<class T>
bool DBContext::Save(std::shared_ptr<T> object) {
    odb::session::current(session);

    odb::core::transaction transaction(database->begin());
    {
        database->persist<T>(object);
    }
    transaction.commit();

    return true;
}

template<typename T>
bool DBContext::Update(std::shared_ptr<T> object) {
    odb::session::current(session);

    odb::core::transaction transaction(database->begin());
    {
        database->update<T>(object);
    }
    transaction.commit();

    return true;
}

template<typename T>
bool DBContext::Delete(uint64_t id) {
    odb::session::current(session);

    odb::core::transaction transaction(database->begin());
    {
        database->erase<T>(id);
    }
    transaction.commit();

    return true;
}

template<typename T>
std::shared_ptr<T> DBContext::QueryOne(const odb::query<T> &query) {
    odb::session::current(session);

    std::shared_ptr<T> result = nullptr;
    odb::core::transaction transaction(database->begin());
    {
        result = database->query_one<T>(query);
    }
    transaction.commit();

    return result;
}

template<typename T>
std::vector<std::shared_ptr<T>> DBContext::Query(const odb::query<T> &query) {
    odb::session::current(session);

    std::vector<std::shared_ptr<T>> result;

    odb::core::transaction transaction(database->begin());
    {
        for (T object : database->query<T>(query)) {
            result.emplace_back(new T(object));
        }
    }
    transaction.commit();

    return result;
}