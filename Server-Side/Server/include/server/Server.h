#pragma once

#include <boost/asio.hpp>
#include <memory>
#include <cmanager/ICommandManager.h>
#include "ConnectionManager.h"
#include "Connection.h"
#include "IOServiceManager.h"

class Server {
public:
    Server(const std::shared_ptr<ICommandManager> &commandManager, uint16_t port);

    virtual ~Server();

    void Run();

private:
    void wait();

    void accept();

    IOServiceManager ioServiceManager;
    ConnectionManager connectionManager;

    std::shared_ptr<Connection> incomingConnection;

    std::shared_ptr<ICommandManager> commandManager;

    boost::asio::ip::tcp::acceptor acceptor;
    boost::asio::signal_set signals;
};
