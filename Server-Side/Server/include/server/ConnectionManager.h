#pragma once

#include <memory>
#include <mutex>
#include <unordered_set>
#include "Connection.h"

class ConnectionManager {
public:
    ConnectionManager();

    ConnectionManager(const ConnectionManager &) = delete;

    ConnectionManager &operator=(const ConnectionManager &) = delete;

    void StartConnection(const std::shared_ptr<Connection> &connection);

    void StopConnection(const std::shared_ptr<Connection> &connection);

    void StopAll();

private:
    std::unordered_set<std::shared_ptr<Connection>> connections;
    std::mutex mutex;
};
