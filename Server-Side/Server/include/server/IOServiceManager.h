#pragma once

#include <memory>
#include <boost/asio.hpp>
#include <mutex>

class IOServiceManager {
public:
    IOServiceManager(size_t size);

    IOServiceManager(const IOServiceManager &) = delete;

    IOServiceManager &operator=(const IOServiceManager &) = delete;

    void Run();

    void Stop();

    boost::asio::io_service &BackgroundService();

    boost::asio::io_service &NetworkService();

private:
    std::shared_ptr<boost::asio::io_service> backgroundService;
    std::vector<std::shared_ptr<boost::asio::io_service>> networkServices;
    std::vector<std::shared_ptr<boost::asio::io_service::work>> works;
    size_t next;
    std::mutex mutex;
};
