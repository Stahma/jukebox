#pragma once

#include <boost/asio/ip/tcp.hpp>
#include <string>
#include <boost/asio/streambuf.hpp>
#include <cmanager/ICommandManager.h>

class IOServiceManager;

class ConnectionManager;

class Connection : public std::enable_shared_from_this<Connection> {
public:
    Connection(IOServiceManager &ioServiceManager, ConnectionManager &connectionManager,
               ICommandManager &commandManager);

    Connection(const Connection &) = delete;

    Connection &operator=(const Connection &) = delete;

    virtual ~Connection();

    boost::asio::ip::tcp::socket &Socket();

    void Start();

    void Stop();

private:
    void read();

    void write(std::string data);

    void writeHandler(const boost::system::error_code &error, size_t transferred);

    void process(size_t transferred);

    void readHandler(const boost::system::error_code &error, size_t transferred);

    IOServiceManager &ioServiceManager;
    ConnectionManager &connectionManager;
    ICommandManager &commandManager;
    boost::asio::ip::tcp::socket socket;
    boost::asio::streambuf buffer;
};
