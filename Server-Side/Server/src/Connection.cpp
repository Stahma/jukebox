#include <thread>
#include <ostream>
#include <iostream>
#include "server/Connection.h"
#include "server/ConnectionManager.h"
#include "server/IOServiceManager.h"

Connection::Connection(IOServiceManager &ioServiceManager,
                       ConnectionManager &connectionManager,
                       ICommandManager &commandManager)
        : commandManager(commandManager), ioServiceManager(ioServiceManager),
          connectionManager(connectionManager),
          socket(ioServiceManager.NetworkService()), buffer(1024 * 1024 * 64) { }

Connection::~Connection() { }

boost::asio::ip::tcp::socket &Connection::Socket() {
    return socket;
}

void Connection::Start() {
    read();
}

void Connection::Stop() {
    socket.close();
}

void Connection::read() {
    boost::asio::async_read_until(
            socket, buffer, '\0',
            std::bind(&Connection::readHandler, shared_from_this(),
                      std::placeholders::_1, std::placeholders::_2));
}

void Connection::write(std::string data) {
    buffer.consume(buffer.size());
    buffer.prepare(data.size() + 1);
    buffer.sputn(data.c_str(), data.size() + 1);

    boost::asio::async_write(
            socket, buffer,
            std::bind(&Connection::writeHandler, shared_from_this(),
                      std::placeholders::_1, std::placeholders::_2));
}

void Connection::writeHandler(const boost::system::error_code &error, size_t/* transferred*/) {
    if (error) {
        std::cerr << "Error: " << error.message() << std::endl;
        if (error != boost::asio::error::operation_aborted) {
            connectionManager.StopConnection(shared_from_this());
        }
        return;
    }

    try {
        socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
    connectionManager.StopConnection(shared_from_this());
}

void Connection::readHandler(const boost::system::error_code &error, size_t transferred) {
    if (error) {
        std::cerr << "Error: " << error.message() << std::endl;
        if (error != boost::asio::error::operation_aborted) {
            connectionManager.StopConnection(shared_from_this());
        }
        return;
    }

    ioServiceManager.BackgroundService().post(std::bind(&Connection::process, shared_from_this(), transferred));
}

void Connection::process(size_t transferred) {
    buffer.commit(transferred);
    std::string request((std::istreambuf_iterator<char>(&buffer)), std::istreambuf_iterator<char>());

    std::function<std::string()> function;
    std::string result;
    try {
        function = commandManager.GetSuitableFunction(request);
        result = function();
    } catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    ioServiceManager.NetworkService().post(std::bind(&Connection::write, shared_from_this(), std::move(result)));
}



