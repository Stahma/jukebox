#include <thread>
#include <boost/bind.hpp>
#include "server/IOServiceManager.h"


IOServiceManager::IOServiceManager(size_t size) : next(0) {
    if (size == 0) {
        throw std::runtime_error("There must be at least one service.");
    }

    for (size_t i = 0; i < size; i++) {
        std::shared_ptr<boost::asio::io_service> service(new boost::asio::io_service);
        std::shared_ptr<boost::asio::io_service::work> work(new boost::asio::io_service::work(*service));
        networkServices.emplace_back(service);
        works.emplace_back(work);
    }

    {
        backgroundService = std::make_shared<boost::asio::io_service>();
        std::shared_ptr<boost::asio::io_service::work> work(new boost::asio::io_service::work(*backgroundService));
        works.emplace_back(work);
    }
}

void IOServiceManager::Run() {
    std::vector<std::shared_ptr<std::thread>> threads;

    for (size_t i = 0; i < networkServices.size(); i++) {
        std::shared_ptr<std::thread> thread(
                new std::thread(boost::bind(&boost::asio::io_service::run, networkServices[i]))
        );

        threads.emplace_back(thread);
    }

    for (size_t i = 0; i < 8; i++) {
        std::shared_ptr<std::thread> thread(
                new std::thread(boost::bind(&boost::asio::io_service::run, backgroundService))
        );

        threads.emplace_back(thread);
    }

    for (size_t i = 0; i < threads.size(); i++) {
        threads[i]->join();
    }
}

void IOServiceManager::Stop() {
    for (size_t i = 0; i < networkServices.size(); i++) {
        networkServices[i]->stop();
    }

    backgroundService->stop();
}

boost::asio::io_service &IOServiceManager::NetworkService() {
    std::lock_guard<std::mutex> guard(mutex);

    boost::asio::io_service &service = *(networkServices[next]);
    next = (next + 1) % networkServices.size();

    return service;
}

boost::asio::io_service &IOServiceManager::BackgroundService() {
    return *backgroundService;
}