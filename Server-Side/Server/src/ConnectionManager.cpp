#include "server/ConnectionManager.h"

ConnectionManager::ConnectionManager() { }

void ConnectionManager::StartConnection(const std::shared_ptr<Connection> &connection) {
    {
        std::lock_guard<std::mutex> guard(mutex);
        connections.emplace(connection);
    }

    connection->Start();
}

void ConnectionManager::StopConnection(const std::shared_ptr<Connection> &connection) {
    {
        std::lock_guard<std::mutex> guard(mutex);
        connections.erase(connection);
    }

    connection->Stop();
}

void ConnectionManager::StopAll() {
    {
        std::lock_guard<std::mutex> guard(mutex);

        std::for_each(connections.begin(), connections.end(),
                      std::bind(&Connection::Stop, std::placeholders::_1));

        connections.clear();
    }
}