#include <iostream>
#include <memory>
#include <dbservice/DatabaseService.h>
#include <storage/EllipticsStorage.h>
#include <boost/program_options.hpp>
#include <fstream>
#include <cmanager/CommandManager.h>
#include <repository/PerformerRepository.h>
#include <repository/AlbumRepository.h>
#include <repository/LyricsRepository.h>
#include <repository/GenreRepository.h>
#include <repository/TrackRepository.h>
#include "server/Server.h"

bool parse(int argc, char **argv, std::string &user,
           std::string &password, std::string &database,
           std::string &config, uint16_t &port) {

    namespace options = boost::program_options;
    try {
        options::options_description description("Usage");
        description.add_options()
                           ("help,h", "Show help")
                           ("user,u", options::value<std::string>(&user)->required(), "Database user")
                           ("password,p", options::value<std::string>(&password)->required(), "Database password")
                           ("database,d", options::value<std::string>(&database)->required(), "Database name")
                           ("config,c", options::value<std::string>(&config)->required(), "Storage config")
                           ("port,P", options::value<uint16_t>(&port), "Server port");

        options::variables_map vm;
        options::store(options::parse_command_line(argc, argv, description), vm);

        if (vm.count("help")) {
            std::cout << description << std::endl;
            return false;
        }

        options::notify(vm);
    } catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return false;
    }
    catch (...) {
        std::cerr << "Unknown error!" << std::endl;
        return false;
    }

    return true;
}

int main(int argc, char **argv) {
    std::string user, password, database, path;
    uint16_t port = 3386;
    if (!parse(argc, argv, user, password, database, path, port)) {
        return 1;
    }

    std::ifstream stream(path);
    if (!stream.is_open()) {
        std::cerr << "Can\'t open config." << std::endl;
        return 1;
    }
    std::string config((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
    stream.close();

    std::shared_ptr<DBContext> context(new DBContext(user, password, database));

    std::shared_ptr<IPerformerRepository> performers(new PerformerRepository(context));
    std::shared_ptr<IAlbumRepository> albums(new AlbumRepository(context));
    std::shared_ptr<ITrackRepository> tracks(new TrackRepository(context));
    std::shared_ptr<IGenreRepository> genres(new GenreRepository(context));
    std::shared_ptr<ILyricsRepository> lyrics(new LyricsRepository(context));

    std::shared_ptr<RepositoryContainer> container(new RepositoryContainer());
    container->Register(performers);
    container->Register(albums);
    container->Register(tracks);
    container->Register(genres);
    container->Register(lyrics);

    std::shared_ptr<IDatabaseService> dbservice(new DatabaseService(container));

    std::shared_ptr<IStorageService> sservice;
    try {
        sservice = std::make_shared<EllipticsStorage>(config);
    } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    std::shared_ptr<ICommandManager> commandManager(new CommandManager(dbservice, sservice));

    Server server(commandManager, port);
    server.Run();

    return 0;
}