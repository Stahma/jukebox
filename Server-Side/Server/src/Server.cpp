#include "server/Server.h"

Server::Server(const std::shared_ptr<ICommandManager> &commandManager, uint16_t port)
        : commandManager(commandManager), ioServiceManager(4),
          connectionManager(), acceptor(ioServiceManager.NetworkService()),
          signals(ioServiceManager.NetworkService()) {

    signals.add(SIGINT);
    signals.add(SIGTERM);
#if defined(SIGQUIT)
    signals.add(SIGQUIT);
#endif

    wait();

    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), port);
    acceptor.open(endpoint.protocol());
    acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    acceptor.bind(endpoint);
    acceptor.listen();

    accept();
}

Server::~Server() { }

void Server::Run() {
    ioServiceManager.Run();
}

void Server::accept() {
    incomingConnection.reset(new Connection(ioServiceManager, connectionManager, *commandManager));

    acceptor.async_accept(
            incomingConnection->Socket(),
            [ this ](const boost::system::error_code &error) {
                if (!acceptor.is_open()) {
                    return;
                }

                if (!error) {
                    connectionManager.StartConnection(incomingConnection);
                } else {
                    std::cerr << error.message() << std::endl;
                }

                accept();
            }
    );
}

void Server::wait() {
    signals.async_wait(
            [ this ](const boost::system::error_code &error, int/* sig*/) {
                acceptor.close();
                connectionManager.StopAll();
                ioServiceManager.Stop();
            }
    );
}