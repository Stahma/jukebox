#include "api/Connector.h"
#include "api/Api.h"

#define RAPIDJSON_HAS_STDSTRING 1
#include <rapidjson/writer.h>

static Connector connector;
static ErrorCallback errorCallback = defaultErrorCallback;

static std::string CreateCommandGetById(std::string target, uint64_t id) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        writer.Key("Command");
        writer.String("/get/" + target + "/");

        writer.Key("Data");
        writer.StartObject();
        {
            writer.Key("Id");
            writer.Uint64(id);
        }
        writer.EndObject();
    }
    writer.EndObject();

    return buffer.GetString();
}

static std::string CreateCommandSearchByName(std::string target, std::string name) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        writer.Key("Command");
        writer.String("/search/" + target + "/");

        writer.Key("Data");
        writer.StartObject();
        {
            writer.Key("Name");
            writer.String(name);
        }
        writer.EndObject();
    }
    writer.EndObject();

    return buffer.GetString();
}

static std::string CreateCommandGetByGenre(std::string target, uint64_t id = 0, std::string name = std::string()) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        writer.Key("Command");
        writer.String("/genre/" + target + "/");

        writer.Key("Data");
        writer.StartObject();
        {
            if (id != 0) {
                writer.Key("Id");
                writer.Uint64(id);
            }

            if (!name.empty()) {
                writer.Key("Name");
                writer.String(name);
            }
        }
        writer.EndObject();
    }
    writer.EndObject();

    return buffer.GetString();
}

static std::string CreateCommandFindByName(std::string target, std::string name) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        writer.Key("Command");
        writer.String("/find/" + target + "/");

        writer.Key("Data");
        writer.StartObject();
        {

            writer.Key("Name");
            writer.String(name);
        }
        writer.EndObject();
    }
    writer.EndObject();

    return buffer.GetString();
}


static std::string CreateCommandAll(std::string target) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        writer.Key("Command");
        writer.String("/all/" + target + "/");

        writer.Key("Data");
        writer.StartObject();
        writer.EndObject();
    }
    writer.EndObject();

    return buffer.GetString();
}


void defaultErrorCallback(const char *message) {
    std::cerr << message << std::endl;
}

void Initialize(const char *address, uint16_t port) {
    connector.Start(address, port);
}

void RegisterErrorCallback(ErrorCallback callback) {
    errorCallback = callback;
}

void GetPerformerById(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetById("performer", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetAlbumById(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetById("album", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetTrackById(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetById("track", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetGenreById(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetById("genre", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetLyricsById(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetById("lyrics", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetLinkByKey(const char *key, ResponseCallback responseCallback) {
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    {
        writer.Key("Command");
        writer.String("/get/link/");

        writer.Key("Data");
        writer.StartObject();
        {
            writer.Key("Key");
            writer.String(key);
        }
        writer.EndObject();
    }
    writer.EndObject();

    std::string request = buffer.GetString();
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void SearchPerformersByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandSearchByName("performer", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void SearchAlbumsByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandSearchByName("album", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void SearchTracksByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandSearchByName("track", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void SearchAllByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandSearchByName("all", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void FindPerformersByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandFindByName("performer", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void FindAlbumsByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandFindByName("album", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void FindTracksByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandFindByName("track", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void FindGenreByName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandFindByName("genre", name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetPerformersByGenreId(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("performer", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetAlbumsByGenreId(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("album", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetTracksByGenreId(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("track", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetAllByGenreId(uint64_t id, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("all", id);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetPerformersByGenreName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("performer", 0, name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetAlbumsByGenreName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("album", 0, name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetTracksByGenreName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("track", 0, name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void GetAllByGenreName(const char *name, ResponseCallback responseCallback) {
    std::string request = CreateCommandGetByGenre("all", 0, name);
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void AllPerformers(ResponseCallback responseCallback) {
    std::string request = CreateCommandAll("performer");
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void AllAlbums(ResponseCallback responseCallback) {
    std::string request = CreateCommandAll("album");
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void AllTracks(ResponseCallback responseCallback) {
    std::string request = CreateCommandAll("track");
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void AllGenres(ResponseCallback responseCallback) {
    std::string request = CreateCommandAll("genre");
    connector.SendRequest(std::move(request), responseCallback, errorCallback);
}

void Release() {
    connector.Stop();
}