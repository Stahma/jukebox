#include "api/Request.h"
#include "api/RequestManager.h"
#include <boost/asio/write.hpp>
#include <boost/asio.hpp>

using namespace std::placeholders;


Request::Request(std::shared_ptr<boost::asio::ip::tcp::socket> sock, boost::asio::ip::tcp::endpoint endpoint,
                 RequestManager &requestManager, std::string data, ResponseCallback responseCallback,
                 ErrorCallback errorCallback) : requestManager(requestManager) {

    this->sock = std::move(sock);
    this->endpoint = std::move(endpoint);
    this->responseCallback = responseCallback;
    this->errorCallback = errorCallback;

    buffer.consume(buffer.size());
    buffer.prepare(data.size() + 1);
    buffer.sputn(data.c_str(), data.size() + 1);
}

void Request::Start() {
    sock->async_connect(endpoint, std::bind(&Request::connectHandler, shared_from_this(), _1));
}

void Request::Stop() {
    sock->close();
}

void Request::connectHandler(const boost::system::error_code &error) {
    if (error) {
        requestManager.StopRequest(shared_from_this());
        return errorCallback(error.message().c_str());
    }

    boost::asio::async_write(*sock, buffer, std::bind(&Request::writeHandler, shared_from_this(), _1, _2));
}

void Request::writeHandler(const boost::system::error_code &error, size_t/* transferred*/) {
    if (error) {
        requestManager.StopRequest(shared_from_this());
        return errorCallback(error.message().c_str());
    }

    buffer.consume(buffer.size());
    boost::asio::async_read_until(*sock, buffer, '\0', std::bind(&Request::readHandler, shared_from_this(), _1, _2));
}

void Request::readHandler(const boost::system::error_code &error, size_t transferred) {
    if (error) {
        requestManager.StopRequest(shared_from_this());
        return errorCallback(error.message().c_str());
    }

    buffer.commit(transferred);
    std::string data((std::istreambuf_iterator<char>(&buffer)), std::istreambuf_iterator<char>());

    sock->shutdown(boost::asio::ip::tcp::socket::shutdown_both);

    return responseCallback(data.c_str());
}