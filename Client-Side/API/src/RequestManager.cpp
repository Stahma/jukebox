#include <algorithm>
#include "api/RequestManager.h"
#include "api/Request.h"


RequestManager::RequestManager() { }

RequestManager::~RequestManager() { }

void RequestManager::StartRequest(std::shared_ptr<Request> request) {
    {
        std::lock_guard<std::mutex> guard(mutex);
        requests.emplace(request);
    }

    request->Start();
}

void RequestManager::StopRequest(std::shared_ptr<Request> request) {
    {
        std::lock_guard<std::mutex> guard(mutex);
        requests.erase(request);
    }

    request->Stop();
}

void RequestManager::StopAll() {
    {
        std::lock_guard<std::mutex> guard(mutex);

        std::for_each(requests.begin(), requests.end(),
                      std::bind(&Request::Stop, std::placeholders::_1));

        requests.clear();
    }
}

