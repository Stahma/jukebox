#pragma once

#ifdef __cplusplus
extern "C" {
#endif
typedef void(*ResponseCallback)(const char *);
typedef void(*ErrorCallback)(const char *);
#ifdef __cplusplus
}
#endif
